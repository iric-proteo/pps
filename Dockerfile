FROM python:3.8-buster

# WARNING: Compare processing speed before changing ubuntu image and Python!!

# File Author / Maintainer
MAINTAINER Mathieu Courcelles

# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

# Install packages requirements
RUN apt-get update -qq && \
    apt-get install -y \
            default-jre \
            zip \
    && rm -rf /var/lib/apt/lists/*

# Create directory structure
RUN mkdir -p /usr/app/db \
    && mkdir -p /usr/app/media_root \
    && mkdir -p /usr/app/static \
    && chown -R www-data:www-data /usr/app \
    && mkdir -p /var/www \
    && chown -R www-data:www-data /var/www

# Install Nextflow
USER www-data
RUN mkdir -p /usr/app/bin
WORKDIR /usr/app/bin
RUN curl -s https://get.nextflow.io | bash
USER root

# Copy services
COPY services/gunicorn /usr/app/services/gunicorn
COPY services/pyGeno /usr/app/services/pyGeno

# Prepare Python environment
WORKDIR /usr/app
RUN pip install --no-cache-dir pipenv cython setuptools==52.0.0.0
COPY Pipfile* /usr/app/
RUN /usr/local/bin/pipenv install --system --deploy

WORKDIR /usr/app/services/pyGeno
RUN python setup.py develop

WORKDIR /usr/app

# Copy source code
COPY src /usr/app/src

RUN chown www-data:www-data /usr/app/src/*.sh \
    && chmod 700 /usr/app/src/*.sh \
    && chmod -R 755 *

ARG CI_COMMIT_SHA
ENV GIT_SHA $CI_COMMIT_SHA

WORKDIR /usr/app/src

USER www-data
CMD ["./init.sh"]
