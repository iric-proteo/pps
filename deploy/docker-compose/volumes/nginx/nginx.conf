# Run as a less privileged user for security reasons.
user nginx;

# How many worker threads to run;
# "auto" sets it to the number of CPU cores available in the system, and
# offers the best performance. Don't set it higher than the number of CPU
# cores if changing this parameter.

# The maximum number of connections for Nginx is calculated by:
# max_clients = worker_processes * worker_connections
worker_processes 2;

# Maximum open file descriptors per process;
# should be > worker_connections.
worker_rlimit_nofile 8192;

events {
  # When you need > 8000 * cpu_cores connections, you start optimizing your OS,
  # and this is probably the point at which you hire people who are smarter than
  # you, as this is *a lot* of requests.
  use epoll;
  worker_connections 1024;
  multi_accept on;
}


http {

  include    mime.types;

  default_type application/octet-stream;

  # Hide nginx version information.
  server_tokens off;

  open_file_cache max=1000 inactive=20s;
  open_file_cache_valid 30s;
  open_file_cache_min_uses 5;
  open_file_cache_errors off;

  client_max_body_size 5000M;

  proxy_connect_timeout       518400;
  proxy_send_timeout          518400;
  proxy_read_timeout          518400;
  send_timeout                518400;

  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 65;

  # Enable Gzip
  gzip  on;
  gzip_http_version 1.0;
  gzip_comp_level 2;
  gzip_min_length 1100;
  gzip_buffers     4 8k;
  gzip_proxied any;
  gzip_types text/javascript application/javascript text/css application/json application/xml text/plain;
  gzip_static on;
  gzip_proxied        expired no-cache no-store private auth;
  gzip_disable        "MSIE [1-6]\.";
  gzip_vary           on;

  log_format json escape=json '{ "timestamp": "$time_iso8601", '
  '"remote_address": "$remote_addr", '
  '"x_forwarded_for": "$http_x_forwarded_for", '
  '"request_method": "$request_method", '
  '"url": "$uri", '
  '"query": "$query_string", '
  '"protocol": "$server_protocol", '
  '"status": "$status", '
  '"response_length": "$body_bytes_sent", '
  '"referrer": "$http_referer", '
  '"request_time": "$request_time", '
  '"process_id": "$pid", '
  '"user_agent": "$http_user_agent" }';


  upstream app_server {
    # fail_timeout=0 means we always retry an upstream even if it failed
    # to return a good HTTP response

    # for UNIX domain socket setups
    #server unix:/usr/src/app/tmp/gunicorn.sock fail_timeout=0;

    # for a TCP configuration
    server app:8000 fail_timeout=0;
  }

  server {
    listen 80;

    access_log /dev/stdout json;

    location / {
      proxy_buffers 8 24k;
      proxy_buffer_size 2k;
      proxy_pass http://app_server;
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /nginx-health {
        return 200 "healthy\n";
    }

    location /protected_media/ {
      internal;
      alias /usr/src/app/media_root/;
    }

    location /static/ {
      alias /usr/src/app/static_root/;
      expires 1h;
    }

  }

}
