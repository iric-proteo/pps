# Personalized proteome service

The personalized proteome service can generate protein sequence database
in the FASTA format. It can also check if a peptide sequence differs from the 
reference genome.


## Requirements

This software requires:
 
* Linux operating system (tested on Ubuntu 16.04/18.04 LTS)
* [Docker](http://docs.docker.com/engine/installation/) >= 18.6
* [Docker-compose](https://docs.docker.com/compose/install/) >= 1.24 - for test deployment on a single computer
* [Kubernetes](https://kubernetes.io/) >= 1.13 - for production deployment on a cluster
* [Helm](https://helm.sh/) - only for deployment on a Kubernetes cluster


## Getting started

First, clone this repository:

    $ git clone https://gitlab.com/courcelm/pps
    $ cd pps
    
## Docker-compose deployment

This deployment is suitable for testing the **personalized proteome service** 
on a single computer. It is not configured for performance and **should not be 
considered secure**. It is strongly recommended to change 
default password. 

    $ cd deploy/docker-compose
    $ sudo docker-compose up


To remove the application container

    $ sudo docker-compose down

**Note**: You can skip this deployment if you are using capaMHC 
docker-compose deployment.



## Kubernetes Helm deployment

The Helm chart provided for deployment on Kubernetes is meant as a starting 
point to deploy **personalized proteome services** on your cluster. 
**It should not be considered secure**.  It is strongly recommended to change 
default password. 
It creates Deployments, StatefulSets, HorizontalPodAutoscaler and Services.

It expect cluster nodes with the **volumes** label. Those nodes should have a 
shared network mount (NFS) at **/volumes**. It also use volume claims for
postgres and redis. Please review your default volume policy.

Run the following to deploy the chart:


    $ cd deploy/
        
    $ helm install --namespace pps-test --name pps-test helm/

To remove

    $ helm del --purge pps-test

## Access

You can access the **personalized proteome service** REST API in your 
browser at: [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

**user name:** pps

**password:** change_me_



## Administration

You can manage users at: http://127.0.0.1:8000/admin/

It is strongly recommended to update the default password and token.



## License

LGPL-3.0-or-later

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
Copyright 2015-2020 Mathieu Courcelles

CAPA - Center for Advanced Proteomics Analyses and Thibault's lab

IRIC - Universite de Montreal