"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.apps import AppConfig

# Third party libraries
from health_check.plugins import plugin_dir



# Import project libraries


class PpsApiConfig(AppConfig):
    name = 'api'


    def ready(self):
        from .backends import (
            CeleryHealthCheck,
            MediaBackend,
            PyGenoDataBackend,
        )

        plugin_dir.register(CeleryHealthCheck)
        plugin_dir.register(MediaBackend)
        plugin_dir.register(PyGenoDataBackend)
