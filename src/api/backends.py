"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
import subprocess

# Import Django related libraries
from django.conf import settings

# Third party libraries
from health_check.backends import BaseHealthCheckBackend
from health_check.exceptions import ServiceUnavailable


# Import project libraries


class CeleryHealthCheck(BaseHealthCheckBackend):
    def check_status(self):

        try:
            # Check that the celery worker is online
            cmd = 'celery -A base_site status'
            subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)

        except subprocess.CalledProcessError as e:

            output = e.output.decode()

            if 'redis.exceptions.ConnectionError' in output:
                self.add_error(ServiceUnavailable("Redis"))

            elif 'No nodes replied within time constraint' in output:
                self.add_error(ServiceUnavailable("Worker"))

            else:
                self.add_error(ServiceUnavailable("Unknown error"), e)


class MediaBackend(BaseHealthCheckBackend):
    def check_status(self):
        try:
            if not os.listdir(settings.MEDIA_ROOT):
                raise ServiceUnavailable('Empty')

            if not os.path.exists(os.path.join(settings.MEDIA_ROOT, 'pps')):
                raise ServiceUnavailable('Empty or not initialized')

        except OSError:
            raise ServiceUnavailable('File Not Found Error')

    def identifier(self):
        return 'Media data'


class PyGenoDataBackend(BaseHealthCheckBackend):
    def check_status(self):
        try:

            if not os.listdir(settings.PYGENO_DATA):
                raise ServiceUnavailable('Empty')

            if not os.path.exists(os.path.join(settings.PYGENO_DATA, 'pps')):
                raise ServiceUnavailable('Empty or not initialized')

        except OSError:
            raise ServiceUnavailable('File Not Found Error')

    def identifier(self):
        return 'pyGeno data'
