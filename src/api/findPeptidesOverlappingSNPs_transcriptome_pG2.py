# -*- coding: utf-8 -*-
"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import csv
import locale
import re
import sys
import traceback

from optparse import OptionParser

# Third party library
from pyGeno.configuration import freeDBRegistery
from pyGeno.Genome import Genome
from pyGeno.Protein import Protein
from pyGeno.SNP import AgnosticSNP, CasavaSNP, dbSNPSNP
from pyGeno.SNPFiltering import SNPFilter, SequenceSNP
from pyGeno.tools.BinarySequence import AABinarySequence
from pyGeno.tools.UsefulFunctions import complement, translateDNA
from rabaDB.filters import RabaQuery

# Author info
__author__ = 'jpl'
__email__ = "jean-philippe.laverdure@umontreal.ca"
__copyright__ = "Copyright 2016, Institut de Recherche en Immunologie et en Cancérologie"


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


class Error(Exception):
    def __init__(self, msg):
        self.msg = msg


class MissingPeptideError(Exception):
    def __init__(self, msg):
        self.msg = msg


class Header:
    patt = 'Chromosome\s+number:\s+([0-9]+|[xymXYM]).*Gene\s+symbol:\s+([^;]+),.*Gene\s+id:\s+(ENS\w*G[0-9]+).*Transcript\s+id:\s+(ENS\w*T[0-9]+).*Protein\s+id:\s+([^,]+)'
    compPattern = re.compile(patt)

    def __init__(self, hstr):
        r = self.compPattern.search(hstr)
        self.reset(r.group(1).strip(), r.group(2).strip(), r.group(3).strip(), r.group(4).strip(),
                   r.group(5).strip())

    def reset(self, chro, geneSymbol, gid, tid, pid):
        """self.chromosome = chro
        self.geneSymbol = geneSymbol
        self.geneId = gid
        self.transcriptId = tid
        self.proteinId = pid"""
        self.chro = chro
        self.geneSymbol = geneSymbol
        self.gid = gid
        self.tid = tid
        self.pid = pid
        self.values = {}

    def __str__(self):
        return "Chr %s, gene symbol %s, gene id %s, trans id %s, prot id %s" % (
            self.chro, self.geneSymbol, self.gid, self.tid, self.pid)

    def __getitem__(self, v):
        return self.values[v]

    def __setitem__(self, i, v):
        self.values[i] = v


class QualityFilter(SNPFilter):
    def __init__(self, threshold):
        self.threshold = threshold

    def filter(self, chromosome, **kwargs):

        sources = {}
        alleles = []

        for snp_set, snp in kwargs.items():

            pos = snp.start
            quality_attr = 'quality'

            if isinstance(snp, CasavaSNP):
                quality_attr = 'Qmax_gt'

            if float(getattr(snp, quality_attr)) > self.threshold:
                sources[snp_set] = snp
                alleles.append(snp.alt.replace(',', ''))  # if not an indel append the polymorphism

            # Appends the reference allele to the lot
            alleles.append(snp.ref)
            sources['ref'] = snp.ref

        # Optional we keep a record of the polymorphisms that were used during the process
        return SequenceSNP(alleles, sources=sources)


def build_genome_headers_dict(pg_file):
    headers = []
    seqs = []

    genes = open(pg_file, 'r').read()

    last_line = False

    for line in [line.strip() for line in genes.split('\n')]:
        if len(line):
            if '>' == line[0]:

                header = Header(' '.join(line.split()[1:]))

                if last_line:
                    # This case if for empty sequence in some FASTA file
                    headers[-1] = header
                else:
                    headers.append(header)

                last_line = True
            else:
                seqs.append(line)
                last_line = False

    if len(headers) != len(seqs):
        raise Error('Issue while parsing genome fasta file')

    print('Target Genome Headers Dict Built, moving on...\n')

    return headers, seqs


def peptide_exon_positions_sequences(exons, snps, start, stop):
    positions = list()
    sequences = list()
    sequences_ref = list()

    for exon in exons:

        if exon.CDS_start is None:
            continue

        e_start = exon.CDS_start
        e_end = exon.CDS_end

        if e_start <= start <= e_end:

            e_start = start

            if e_start <= stop <= e_end:
                e_end = stop

        elif start <= e_start <= stop and start <= e_end <= stop:
            pass

        elif e_start <= stop <= e_end:
            e_end = stop

        else:
            continue

        positions_str = '%s-%s' % (e_start + 1, e_end)
        positions.append(positions_str)

        seq_start = e_start - exon.CDS_start
        seq_stop = e_end - exon.CDS_end

        if exon.strand == '-':
            seq_stop = len(exon.CDS) - (e_start - exon.CDS_start)
            seq_start = seq_stop - (e_end - e_start)

        if seq_stop == 0:
            seq_stop = None

        exon_cds_ref = exon.CDS.copy()

        for snp_pos in snps.keys():

            if exon.CDS_start <= snp_pos < exon.CDS_end:

                if exon.strand == '-':
                    nucleotide = complement(list(snps[snp_pos].values())[0].ref)

                    exon_cds_ref[exon.CDS_end-snp_pos-1] = nucleotide
                else:
                    nucleotide = list(snps[snp_pos].values())[0].ref

                    exon_cds_ref[snp_pos - exon.CDS_start] = nucleotide

        sequences.append(''.join(exon.CDS[seq_start:seq_stop]))
        sequences_ref.append(''.join(exon_cds_ref[seq_start:seq_stop]))

    return ';'.join(positions), ';'.join(sequences), ';'.join(sequences_ref)


def extract_non_synonymous_snp(sap_polys, exons, peptide_pos_on_protein, SNPTypes):
    snp_chromosome_positions = list()
    snps = dict()

    # Get SAP position on transcript
    sap_transcript_positions = list()

    for poly in sap_polys:
        sap_pos_on_peptide = poly[0]

        sap_transcript_position = peptide_pos_on_protein * 3 + sap_pos_on_peptide * 3
        sap_transcript_positions.append(sap_transcript_position)

    # Find exons with SAP
    exon_position_on_transcript = 0

    for exon in exons:

        # Skip non coding exon

        if exon.CDS_start is None:
            continue

        for sap_transcript_position in sap_transcript_positions:

            diff = sap_transcript_position - exon_position_on_transcript

            # Check if a SAP is in this exon
            if 0 <= diff <= (exon.CDS_end - exon.CDS_start):

                sap_chromosome_position = None

                if exon.strand == '+':
                    sap_chromosome_position = exon.CDS_start + diff

                elif exon.strand == '-':
                    sap_chromosome_position = exon.CDS_end - diff - 1

                snp_polys = retrieve_snp(SNPTypes, sap_chromosome_position, exon)
                snps.update(snp_polys)

                for key, value in snp_polys.items():
                    # print('key', key)
                    snp_chromosome_positions.append(key)

        exon_position_on_transcript += exon.CDS_end - exon.CDS_start

    return snp_chromosome_positions, snps


def retrieve_snp(SNPTypes, sap_chromosome_position, exon):
    iterators = []
    for setName, SNPType in SNPTypes.items():
        f = RabaQuery(str(SNPType), namespace=exon.chromosome._raba_namespace)

        # Fix for mitochondrial chromosome
        # Inconsistent label between Chromosome and SNP
        chromosome_number = exon.chromosome.number

        if chromosome_number == 'MT':
            chromosome_number = 'M'

        f.addFilter({'start >=': sap_chromosome_position - 3,
                     'start <': sap_chromosome_position + 3,
                     'setName': str(setName),
                     'chromosomeNumber': chromosome_number})
        # conf.db.enableDebug(True)
        iterators.append(f.iterRun(sqlTail='ORDER BY start'))

    snp_polys = {}
    for iterator in iterators:
        for poly in iterator:

            set_polys = {poly.setName: poly}

            sequence_modifier = exon.chromosome.genome.SNPFilter.filter(exon.chromosome, **set_polys)

            if sequence_modifier is None:
                continue

            if poly.start not in snp_polys:
                snp_polys[poly.start] = {poly.setName: poly}
            else:
                snp_polys[poly.start][poly.setName] = poly

    return snp_polys


def format_sap_polys(polys, peptide, ref_peptide):
    polys_formatted = list()

    ref_peptide = list(ref_peptide)
    var_peptide = list(peptide)

    for i, residues in enumerate(zip(ref_peptide, var_peptide)):

        if residues[0] != residues[1]:
            polys_formatted.append(
                (i + 1, '%s->%s' % (residues[0], residues[1])))

    for poly in polys:
        pos, s = poly

        sap_residues = list(s)

        var_peptide[pos] = '%s/%s' % (sap_residues[0], sap_residues[1])

    return polys_formatted, ''.join(var_peptide)


def parse_and_output(peptide, h, pep_pos, perso_genome):
    bedlines = []
    outputlines = []

    pg_prot = perso_genome.get(Protein, id=h.pid)[0]

    for pos in pep_pos:

        variantSeq = pg_prot.bin_sequence.decode(pg_prot.bin_sequence[pos:(pos + len(peptide))])
        variantPep = AABinarySequence(variantSeq)
        polys = variantPep.getPolymorphisms()

        transcr = pg_prot.transcript
        gene = transcr.gene
        strand = gene.strand
        utr = transcr.UTR5

        cdna_start_pos = pos * 3
        cdna_stop_pos = (pos + len(peptide)) * 3
        tr_start_pos = cdna_start_pos + len(utr)
        tr_stop_pos = cdna_stop_pos + len(utr)
        size = 0

        positions = list()

        # Extract non-synonymous SNP position
        snp_chromosome_positions_str = ''
        snps = dict()

        if len(polys):
            snp_chromosome_positions, snps = extract_non_synonymous_snp(polys, transcr.exons, pos, perso_genome.SNPTypes)

            snp_chromosome_positions = [str(x + 1) for x in snp_chromosome_positions]
            snp_chromosome_positions_str = ';'.join(snp_chromosome_positions)

        for exon in transcr.exons:

            if size == 0 and exon.frame is not None and exon.frame != 0:
                size -= exon.frame

            size += len(exon)

            if size >= (tr_start_pos + (
                    tr_stop_pos - tr_start_pos)):  # dont forget >=, if not, you wont correctly place peptide finishing right at the end of the exon

                if len(positions):
                    if strand == '+':
                        gen_start_pos = exon.CDS_start
                        gen_stop_pos = exon.end - (size - (tr_start_pos + (tr_stop_pos - tr_start_pos)))
                    if strand == '-':
                        gen_start_pos = exon.start + (size - (tr_start_pos + (tr_stop_pos - tr_start_pos)))
                        gen_stop_pos = exon.CDS_end
                else:
                    if strand == '+':
                        gen_start_pos = exon.end - (size - tr_start_pos)
                        gen_stop_pos = exon.end - (size - (tr_start_pos + (tr_stop_pos - tr_start_pos)))
                    if strand == '-':
                        gen_start_pos = exon.start + (size - (tr_start_pos + (tr_stop_pos - tr_start_pos)))
                        gen_stop_pos = exon.start + (size - tr_start_pos)

                positions.append((gen_start_pos, gen_stop_pos))

                break

            if size > tr_start_pos and size < tr_stop_pos:  # >= in case the last base of the exon encodes the beginning of the peptide (2 other nt of the codon
                # being on the following exon)

                if len(positions):
                    positions.append((exon.start, exon.end))

                else:
                    if strand == '+':
                        gen_start_pos_1 = exon.end - (size - tr_start_pos)
                        gen_stop_pos_1 = exon.end

                        positions.append((gen_start_pos_1, gen_stop_pos_1))
                    if strand == '-':
                        gen_start_pos_2 = exon.start
                        gen_stop_pos_2 = exon.start + (size - tr_start_pos)

                        positions.append((gen_start_pos_2, gen_stop_pos_2))

        if strand == '+':
            gen_start_pos = positions[0][0]
            gen_stop_pos = positions[-1][1]
        else:
            gen_start_pos = positions[-1][0]
            gen_stop_pos = positions[0][1]

        exon_positions, exon_sequences, exon_sequences_ref = peptide_exon_positions_sequences(transcr.exons, snps, gen_start_pos, gen_stop_pos)

        block_sizes = [str(stop - start) for start, stop in positions]
        block_starts = [str(start - gen_start_pos) for start, stop in positions]

        # Generate output
        bed = ['chr{0}'.format(gene.chromosome.number),
                         str(gen_start_pos), str(gen_stop_pos),
                         peptide, '900', gene.strand, str(gen_start_pos),
                         str(gen_stop_pos), '0', str(len(positions)),
                         ','.join(block_sizes),
                         ','.join(block_starts)]

        bedlines.append(bed)

        locus = 'chr{0}:{1}-{2}'.format(h.chro, locale.format_string("%d", gen_start_pos + 1, grouping=True),
                                        locale.format_string("%d", gen_stop_pos, grouping=True))

        outputline = ['{0}'.format(h.geneSymbol),
                      '{0}'.format(gene.id),
                      '{0}'.format(transcr.id),
                      '{0}'.format(h.pid),
                      str(pos + 1),
                      '{0}'.format(h.chro),
                      strand,
                      locus,
                      exon_positions,
                      exon_sequences,
                      exon_sequences_ref,
                      '\t'.join(bed)
                      ]

        if len(polys):

            if h.chro != 'MT':
                ref_peptide = translateDNA(exon_sequences_ref.replace(';', ''))
            else:
                ref_peptide = translateDNA(exon_sequences_ref.replace(';', ''),
                                           translTable_id='mt')

            # Handle reference peptide with variants
            if '/' in ref_peptide:

                for pattern in ['\/.', '.\/']:
                    tmp_ref_peptide = re.sub(pattern, '', ref_peptide)

                    if tmp_ref_peptide == peptide:
                        break

                ref_peptide = tmp_ref_peptide

            is_ref_peptide = ref_peptide == peptide

            polys_formatted, variantSeq = format_sap_polys(polys, peptide, ref_peptide)

            outputline.append(str(polys_formatted))
            outputline.append(variantSeq)
            outputline.append(snp_chromosome_positions_str)
            outputline.append(ref_peptide)
            outputline.append(is_ref_peptide)

        else:
            for _ in range(0, 3):
                outputline.append('')
            outputline.append(peptide)
            outputline.append(True)


        outputlines.append(outputline)

    return bedlines, outputlines


def find_peptide_position(protein_sequence, peptide_sequence):
    offset = 0
    positions = list()
    i = protein_sequence.find(peptide_sequence, offset)

    while i >= 0:
        positions.append(i)
        i = protein_sequence.find(peptide_sequence, i + 1)

    return positions


def main():


    pep_col = -1
    delimiter = ','

    try:
        usage = "usage: %prog [options] <PG.fasta> <proteomics_identifications_file.csv>"  # review with extra args
        parser = OptionParser(usage)

        # Define optparse arguments, see https://docs.python.org/2/library/optparse.html
        # option processing -- Please revise accordingly !
        parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
                          help="Enable verbose mode")

        parser.add_option('-d', '--dbsnp', dest='dbsnp', help='SNPs', default='dbSNP138')
        parser.add_option('-t', '--quality-threshold', dest='quality_threshold', help='quality_threshold', default='20')
        parser.add_option('-g', '--genome', dest='genome_id', help='Genome to load', default='GRCm38.78')
        parser.add_option('-c', '--chromosome', dest='chromosome_id', help='Chromosome', default=None)
        parser.add_option('-s', '--species', dest='species', help='Species of specified genome', default='mouse')
        parser.add_option('-p', '--peptideColumn', dest='pep_col_token', help='Header of peptide column',
                          default='Peptide')

        (options, args) = parser.parse_args()

        print('Analysis details:')
        print('\tgenome header input file: ', args[0])
        print('\tpeptide identification input file: ', args[1])
        print('\tgenome: ', options.genome_id)
        print('\tSNPs: ', options.dbsnp)

        with open(args[1], 'r') as infile:

            reader = csv.reader(infile, delimiter=delimiter)

            # Find appropriate columns and save file headers
            csv_headers = next(reader)
            col_index = 0

            for col in csv_headers:

                if options.pep_col_token == col:
                    pep_col = col_index

                col_index += 1

            if -1 == pep_col:
                raise Error('Input CSV file is missing some of the necessary columns\n'
                            '{0}: {1}'.format(options.pep_col_token, pep_col))

            ###
            # All columns accounted for, Keep On Truckin'
            ###

            ##
            # Load genome and fetch PG data
            # ref_genome = Genome(species='mouse', name='GRCm38.78', )
            snp_filter = QualityFilter(int(options.quality_threshold))

            perso_genome = Genome(species=options.species, name=options.genome_id, SNPs=options.dbsnp,
                                  SNPFilter=snp_filter)

            bag = list(perso_genome._bags.values())[0]
            keywords = ['Gene', 'Transcript', 'Exon', 'Protein']

            headers, seqs = build_genome_headers_dict(args[0])

            bedname = args[2].replace('.csv', '.bed')

            with open(bedname, 'w') as bed, open(args[2], 'w') as output:

                # write all file headers
                output.write(','.join(csv_headers + ['mapped',
                                                     'gene_symbol',
                                                     'gene_identifier',
                                                     'transcript_identifier',
                                                     'protein_identifier',
                                                     'protein_position',
                                                     'chromosome',
                                                     'strand',
                                                     'locus', 'exon_positions',
                                                     'exon_sequences',
                                                     'exon_sequences_ref',
                                                     'bed',
                                                     'sap', 'peptide_variants',
                                                     'snp_positions',
                                                     'ref_peptide',
                                                     'is_ref_peptide'
                                                     ]) + '\n')

                source_genes = set()

                if options.chromosome_id == 'MT':
                    options.chromosome_id = 'M'

                for header, sequence in zip(headers, seqs):

                    if options.chromosome_id is not None:

                        if options.chromosome_id != header.chro:

                            continue

                    for row in reader:
                        if len(row) != len(csv_headers):
                            row += ['' for _ in range(len(csv_headers) - len(row))]

                        peptide = row[pep_col]

                        # Looking for source proteins for peptide
                        positions = find_peptide_position(sequence, peptide)

                        if len(positions):

                            if (peptide, header.geneSymbol) not in source_genes:  # only output the first of isoforms
                                bedlines, outputlines = parse_and_output(peptide, header, positions, perso_genome)

                                # Start standard output
                                for bedline in bedlines:
                                    bed.write('{0}\n'.format('\t'.join(bedline)))

                                for outputline in outputlines:

                                    if len(row) != len(csv_headers):
                                        row += ['' for _ in range(len(csv_headers) - len(row))]

                                    output.write('{0},{1},{2}\n'.format(','.join(['"' + str(f) + '"' for f in row]),
                                                                        True,
                                                                        ','.join(['"' + str(f) + '"' for f in
                                                                                  outputline])))

                            source_genes.add((peptide, header.geneSymbol))

                    # Clean pygeno cache
                    bag_keys = list(bag.keys())
                    for key in bag_keys:

                        if any(keyword in key[0] for keyword in keywords):
                            del (bag[key])

                    freeDBRegistery()

                    infile.seek(0)
                    infile.readline()

        print('Successful completion !!')

    except Usage as err:
        print('', file=sys.stderr)
        print(sys.argv[0].split('/')[-1] + ': ' + str(err.msg))
        print('\t for help use --help', file=sys.stderr)
        return 2

    except Error as err:
        print('', file=sys.stderr)
        print(sys.argv[0].split('/')[-1] + ': ' + str(err.msg), file=sys.stderr)
        print('\t for help use --help', file=sys.stderr)
        return 1

    except Exception:
        print('', file=sys.stderr)
        print(traceback.format_exc(2), file=sys.stderr)
        print(__doc__, file=sys.stderr)
        return 1


if __name__ == '__main__':
    sys.exit(main())