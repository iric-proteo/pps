"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from rest_framework.authtoken.models import Token

# Import project libraries


class Command(BaseCommand):
    help = 'Init pps'

    def handle(self, *args, **options):
        """
        Creates a new user and displays the token.
        :param args: List arguments
        :param options: Command line arguments dictionary
        :return: None
        """

        User = get_user_model()

        user = User.objects.create_user(username='pps',
                                        email='change@me.com',
                                        is_superuser=True,
                                        is_staff=True)
        user.set_password('change_me_')
        user.save()

        user.auth_token.delete()

        user.auth_token = Token(key='change_me_', user=user)
        user.auth_token.save()
