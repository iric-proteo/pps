"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from configparser import ConfigParser
import gzip
from pathlib import Path
import tarfile


# Third party library


# Import project libraries


def split_gtf(gtf_location: str) -> None:
    """
    Download and split GTF file by chromosome
    :param gtf_location: GTF location provided by the genome data wrap
    :return: Write gtf.gz file for each chromosome
    """

    print('Downloading GTF...')
    from pyGeno.importation.Genomes import _getFile
    gtf_file = _getFile(gtf_location, '.')

    print('Splitting GTF by chromosome...')
    current_chromosome = None
    out_file = None

    with gzip.open(gtf_file, 'r') as fh_in:

        for line in fh_in:
            line = line.decode()

            if line.startswith('#'):
                continue

            chromosome = line[0:2].rstrip('\t')

            if current_chromosome != chromosome:
                print(f'-{chromosome}-')

                current_chromosome = chromosome

                if out_file is not None:
                    out_file.close()

                out_file = gzip.open(f'{chromosome}.gtf.gz', 'wt')

            out_file.write(line)

        out_file.close()

    Path(gtf_file).unlink()


def chromosome_manifest(data_wrap_file: str) -> None:
    """
    Reads pygeno data_wrap manifest.ini for genome and split it for each
    chromosome

    :param data_wrap_file: pygeno data_wrap file
    :return: Write manifest archive files
    """

    config = ConfigParser()

    with tarfile.open(data_wrap_file, mode='r:gz') as fh_archive:
        fh_manifest = fh_archive.extractfile('manifest.ini')
        config.read_string(fh_manifest.read().decode())

    if not config['gene_set']['gtf'].startswith('ftp') and \
            not config['gene_set']['gtf'].startswith('http'):

        with tarfile.open(data_wrap_file, mode='r:gz') as fh_archive:
            fh_archive.extract(config['gene_set']['gtf'])

    split_gtf(config['gene_set']['gtf'])

    section = config['chromosome_files']
    values = {option: section[option] for option in section}

    print('Assembling chromosome data wrap...')
    for option in section:
        print(f'-{option}-')

        config.remove_section('chromosome_files')
        config.add_section('chromosome_files')

        config['chromosome_files'][option] = values[option]
        config['gene_set']['gtf'] = f'{option.upper()}.gtf.gz'

        with open('manifest.ini', 'w') as fh_out:
            config.write(fh_out)

        with tarfile.open(f'manifest_{option.upper()}.tar.gz',
                          'w:gz') as tar_gz_out:

            for file_ in ['manifest.ini', f'{option.upper()}.gtf.gz']:
                tar_gz_out.add(file_)
                Path(file_).unlink()

            if not values[option].startswith('http') and \
                not values[option].startswith('ftp'):

                with tarfile.open(data_wrap_file, mode='r:gz') as fh_archive:
                    fh_archive.extract(config['chromosome_files'][option])
                    tar_gz_out.add(values[option])
                    Path(values[option]).unlink()

    print('Task complete.')


def read_manifest(data_wrap_file: str) -> ConfigParser:
    """
    Reads pygeno data_wrap manifest.ini for genome and snps

    :param data_wrap_file: pygeno data_wrap file
    :return: Manifest configuration
    """

    config = ConfigParser()

    with tarfile.open(data_wrap_file, mode='r:gz') as fh_archive:
        fh_manifest = fh_archive.extractfile('manifest.ini')
        config.read_string(fh_manifest.read().decode())

    return config
