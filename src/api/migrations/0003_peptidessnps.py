# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-10-05 16:41
from __future__ import unicode_literals

import api.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20160929_1017'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeptidesSNPs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('peptides_file', models.FileField(upload_to=api.models.upload_path_handler)),
                ('status', models.CharField(choices=[(b'', b''), (b'pending', b'pending'), (b'started', b'started'), (b'finished', b'finished'), (b'failed', b'failed')], default=b'', max_length=10)),
                ('log_file', models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler_pp)),
                ('out_file', models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler_pp)),
                ('personalized_proteome', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.PersonalizedProteome')),
            ],
        ),
    ]
