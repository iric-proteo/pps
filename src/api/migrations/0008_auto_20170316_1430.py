# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-03-16 18:30
from __future__ import unicode_literals

import api.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20161215_0935'),
    ]

    operations = [
        migrations.AddField(
            model_name='personalizedproteome',
            name='expression_file',
            field=models.FileField(blank=True, help_text=b'Kallisto format', null=True, upload_to=api.models.upload_path_handler_pp),
        ),
        migrations.AddField(
            model_name='personalizedproteome',
            name='tpm_threshold',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
