# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-07-12 17:38
from __future__ import unicode_literals

import api.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0018_auto_20170712_0821'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peptidessnps',
            name='bed_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='peptidessnps',
            name='csv_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='peptidessnps',
            name='log_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='peptidessnps',
            name='peptides_file',
            field=models.FileField(upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='personalizedproteome',
            name='errors_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='personalizedproteome',
            name='expression_file',
            field=models.FileField(blank=True, help_text=b'Kallisto format', null=True, upload_to=api.models.upload_path_handler),
        ),
        migrations.AlterField(
            model_name='personalizedproteome',
            name='fasta_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.upload_path_handler),
        ),
    ]
