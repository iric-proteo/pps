"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from pathlib import Path

# Import Django related libraries
from django.conf import settings
from django.db import models
from django.db.models.signals import (post_save,
                                      pre_save)
from django.dispatch import receiver
from django.utils.functional import cached_property

from rest_framework.authtoken.models import Token

# Import project libraries

# Statuses for tasks
STATUSES = (
    ('', ''),
    ('pending', 'pending'),
    ('started', 'started'),
    ('finished', 'finished'),
    ('failed', 'failed'),
    ('uploaded', 'uploaded'),
)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def upload_path_handler_pp(instance, file_name):
    # This is here to keep migration working
    pass


def upload_path_handler(instance, file_name):
    """
    Path handler for file upload.
    :param instance: Model instance
    :param file_name: String
    :return: String
    """

    file_path = os.path.join(type(instance).__name__, str(instance.pk), file_name)

    if os.path.exists(file_path):
        raise OSError('File exists at: %s' % file_path)

    return file_path


# Code for file upload
# from http://stackoverflow.com/questions/9968532/django-admin-file-upload-with-current-model-id
_UNSAVED_FILEFIELD = 'unsaved_filefield'


@receiver(pre_save)
def skip_saving_file(sender, instance, **kwargs):
    """
    Waits that object has been saved before saving files.
    :param sender: Model class
    :param instance: Model instance
    :param kwargs: Keyword arguments
    :return: None
    """

    if sender in [Genome, SNPs]:
        if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
            setattr(instance, _UNSAVED_FILEFIELD, instance.file)
            instance.file = None

    elif sender in [PeptidesSNPs]:
        if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
            setattr(instance, _UNSAVED_FILEFIELD, instance.peptides_file)
            instance.peptides_file = None

    elif sender in [PersonalizedProteome]:
        if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
            setattr(instance, _UNSAVED_FILEFIELD, instance.expression_file)
            instance.expression_file = None


@receiver(post_save)
def save_file(sender, instance, created, **kwargs):
    """
    Saves the files now that the object has been saved.
    :param sender: Model class
    :param instance: Model instance
    :param created: Boolean
    :param kwargs: Keyword arguments
    :return: None
    """

    if sender in [Genome, SNPs]:
        if created and hasattr(instance, _UNSAVED_FILEFIELD):
            instance.file = getattr(instance, _UNSAVED_FILEFIELD)
            instance.save()

    elif sender in [PeptidesSNPs]:
        if created and hasattr(instance, _UNSAVED_FILEFIELD):
            instance.peptides_file = getattr(instance, _UNSAVED_FILEFIELD)
            instance.save()

    elif sender in [PersonalizedProteome]:
        if created and hasattr(instance, _UNSAVED_FILEFIELD):
            instance.expression_file = getattr(instance, _UNSAVED_FILEFIELD)
            instance.save()


class DataWrapBase(models.Model):
    """
    Base model for Genome and SNPs model.
    """

    name = models.CharField(blank=True,
                            max_length=200,
                            null=True)

    file = models.FileField(help_text='pyGeno data wrap file',
                            upload_to=upload_path_handler)

    import_status = models.CharField(choices=STATUSES,
                                     default='pending',
                                     max_length=10)

    import_logs = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True,
                                      null=True)

    comment = models.TextField(blank=True, null=True)

    trashed = models.BooleanField(default=False)

    git_sha = models.CharField(blank=True,
                               max_length=40,
                               null=True)

    class Meta:
        abstract = True


class Genome(DataWrapBase):
    """
    Model for Genome data wrap
    """

    species = models.CharField(blank=True,
                               max_length=200,
                               null=True)

    def __str__(self):
        """
        String representation of instance.
        :return: String
        """

        return f'{self.name} ({self.species})'


class SNPs(DataWrapBase):
    """
    Model for SNPs data wrap
    """

    genome = models.ForeignKey(Genome,
                               help_text='Reference Genome associated'
                                         ' to this SNPs data set.',
                               on_delete=models.PROTECT)

    def __str__(self):
        """
        String representation of instance.
        :return: String
        """

        return f'{self.name} [{self.genome}]'

    @property
    def genome_name(self):
        return str(self.genome)

    @property
    def species(self):
        return str(self.genome.species)


class PersonalizedProteome(models.Model):
    """
    Model for Personalized proteome database
    """

    snps = models.ForeignKey(SNPs,
                             on_delete=models.PROTECT)

    quality_threshold = models.PositiveIntegerField(default=20,
                                                    help_text='>')

    expression_file = models.FileField(blank=True,
                                       help_text='Kallisto format',
                                       upload_to=upload_path_handler,
                                       null=True)

    tpm_threshold = models.FloatField(blank=True,
                                      help_text='>',
                                      null=True)

    protein_length_threshold = models.PositiveIntegerField(default=10000,
                                                           help_text='<=')

    remove_redundant = models.BooleanField(default=False,
                                           verbose_name='Remove redundant sequences')

    fasta_file = models.FileField(blank=True,
                                  upload_to=upload_path_handler,
                                  null=True)

    errors_file = models.FileField(blank=True,
                                   upload_to=upload_path_handler,
                                   null=True)

    fasta_status = models.CharField(choices=STATUSES,
                                    default='',
                                    max_length=10)

    created_at = models.DateTimeField(auto_now_add=True,
                                      null=True)

    comment = models.TextField(blank=True, null=True)

    trashed = models.BooleanField(default=False)

    git_sha = models.CharField(blank=True,
                               max_length=40,
                               null=True)

    sample_id = models.CharField(default='', max_length=20)

    reformat = models.BooleanField(default=False)

    def __str__(self):
        """
        String representation of instance.
        :return: String
        """

        return '%s [%s]' % (self.snps.name, str(self.snps.genome))

    @property
    def snps_id_(self):
        return self.snps_id

    @property
    def snps_name(self):
        return str(self.snps)


class PeptidesSNPs(models.Model):
    personalized_proteome = models.ForeignKey(PersonalizedProteome,
                                              on_delete=models.PROTECT)

    peptides_file = models.FileField(upload_to=upload_path_handler)

    peptide_column = models.CharField(default='Peptide sequence',
                                      max_length=50)

    status = models.CharField(choices=STATUSES,
                              default='',
                              max_length=10)

    log_file = models.FileField(blank=True,
                                upload_to=upload_path_handler,
                                null=True)

    csv_file = models.FileField(blank=True,
                                upload_to=upload_path_handler,
                                null=True)

    bed_file = models.FileField(blank=True,
                                upload_to=upload_path_handler,
                                null=True)

    created_at = models.DateTimeField(auto_now_add=True,
                                      null=True)

    trashed = models.BooleanField(default=False)

    comment = models.TextField(blank=True, null=True)

    git_sha = models.CharField(blank=True,
                               max_length=40,
                               null=True)

    @property
    def peptides_file_name(self):
        return os.path.basename(self.peptides_file.name)

    @property
    def personalized_proteome_pk(self):
        return self.personalized_proteome_id

    @property
    def personalized_proteome_snps_name(self):
        return self.personalized_proteome.snps_name
