"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from collections import defaultdict
import sys

# Third party library
import begin

from pyGeno.Chromosome import Chromosome
from pyGeno.configuration import freeDBRegistery
import pyGeno.importation.Genomes as genomes
import pyGeno.importation.SNPs as snps
from pyGeno.Protein import Protein
from pyGeno.SNP import AgnosticSNP, CasavaSNP
from pyGeno.SNPFiltering import SNPFilter, SequenceSNP


# Import project libraries
from manifest import chromosome_manifest # Import ok even if pycharm warning


@begin.subcommand
def import_genome(genome_file):
    """
    Imports genome data wrap to pyGeno database
    :param genome_file: File name (String)
    :return: None
    """

    genomes.importGenome(genome_file, batchSize=500)


@begin.subcommand
def import_snps(snps_file: str, chromosome: str):
    """
    Imports snps data wrap to pyGeno database.
    :param snps_file: File name
    :param chromosome: Chromosome
    :return: None
    """

    if chromosome == 'MT':
        chromosome = 'M'

    snps.importSNPs(snps_file, chromosome)


@begin.subcommand
def make_personalized_proteome(genome_name, snps_name, chromosome,
                               quality_threshold,
                               expression_file=None, tpm_threshold=None,
                               protein_length_threshold=None):
    """
    Produces a personalized proteome.
    """

    """
    Data should have already been imported inside pyGeno2 instance

    Adapted from makePersonalisedGenome_pG2.py script written by
    Jean-Philippe Laverdure and Tariq Daouda(IRIC)
    :param genome_name: String
    :param snps_name: String
    :param chromosome: Chromosome number, can be set to All (String)
    :param quality_threshold: Drops SNPs with quality lower than (Float)
    :param expression_file: Expression file in Kalisto format
    :param tpm_threshold: Drops transcript with TPM lower than (Float)
    :param protein_length_threshold: Drops protein with length longer than (Integer)
    :return: None
    """

    def sublist_polymorphisms(polymorphisms, distance=20, max_length=8):
        """
        Fragment polymorphisms list into sublist
        :param polymorphisms: List
        :param distance: Distance between subset (Integer)
        :param max_length: Maximum size of subset (Integer)
        :return: List of polymorphisms list
        """

        positions = list()
        subsets = list()

        for polymorphism in polymorphisms:

            position = polymorphism[0]

            if len(positions) != 0:

                diff = position - positions[-1]

                if diff > distance:
                    subsets.append(positions)
                    positions = list()

                if len(positions) == max_length:
                    subsets.append(positions)
                    positions.pop(0)

            positions.append(position)

        subsets.append(positions)

        subset_polymorphisms = list()
        polymorphisms_tmp = list()

        for positions in subsets:

            for polymorphism in polymorphisms:

                position = polymorphism[0]

                if position in positions:
                    polymorphisms_tmp.append(polymorphism)

                else:
                    pos_polymorphisms = sorted(list(polymorphism[1]))

                    if pos_polymorphisms[0] == '*' and len(
                            pos_polymorphisms) > 1:
                        pos_polymorphisms.pop(0)
                    polymorphisms_tmp.append(
                        (polymorphism[0], set(pos_polymorphisms[0])))

            subset_polymorphisms.append(polymorphisms_tmp)
            polymorphisms_tmp = list()

        return subset_polymorphisms

    quality_threshold = int(quality_threshold)
    protein_length_threshold = int(protein_length_threshold)

    keep_identifiers = set()

    if expression_file is not None and tpm_threshold is not None:

        tpm_threshold = float(tpm_threshold)

        with open(expression_file, 'r') as f:

            header_fields = f.readline().rstrip().split('\t')

            target_id_index = header_fields.index('target_id')
            tpm_index = header_fields.index('tpm')

            for line in f:

                fields = line.rstrip().split('\t')

                if float(fields[tpm_index]) > tpm_threshold:
                    target_id = fields[target_id_index].split('.')[0]
                    keep_identifiers.add(target_id)

    class QualityFilter(SNPFilter):

        def __init__(self, threshold):
            self.threshold = threshold

        def filter(self, chromosome, **kwargs):

            for snp_set, snp in kwargs.items():

                if isinstance(snp, CasavaSNP):

                    if snp.Qmax_gt > self.threshold:
                        return SequenceSNP(snp.alt)

                elif isinstance(snp, AgnosticSNP):

                    if float(snp.quality) > self.threshold:
                        return SequenceSNP(snp.alt.replace(',', ''))

            return None

    base_max_variants = 1024
    max_variants = base_max_variants * 1

    header_template = ">{0}_{1} Chromosome number: {0} | " \
                      "Gene symbol: {2}, Gene id: {3} | Transcript id: {4} | " \
                      "Protein id: {5}\n{6}"

    snp_filter = QualityFilter(quality_threshold)

    genome = genomes.Genome(name=str(genome_name), SNPs=str(snps_name),
                            SNPFilter=snp_filter)

    discard = defaultdict(int)
    discard_list = defaultdict(list)

    chromosomes = genome.get(Chromosome, number=chromosome)

    bag = list(genome._bags.values())[0]

    keywords = ['Gene', 'Transcript', 'Exon', 'Protein']

    if chromosome == 'All':
        chromosomes = genome.iterGet(Chromosome)

    for chrom in chromosomes:

        pid = 1

        for protein in chrom.iterGet(Protein):

            transcript = protein.transcript

            # Discard transcript below expression threshold
            if len(keep_identifiers) and transcript.id not in keep_identifiers:
                discard['expression'] += 1
                discard_list['expression'].append(protein.id)
                continue

            gene = protein.gene

            variant_seen = set()

            for sub_polymorphisms in sublist_polymorphisms(
                    protein.bin_sequence.polymorphisms):

                protein.bin_sequence.polymorphisms = sub_polymorphisms

                alts = protein.bin_sequence.getSequenceVariants(
                    maxVariantNumber=max_variants)

                if not alts[0]:

                    for alt in alts[1]:

                        if '*' in alt:
                            if transcript.biotype == 'polymorphic_pseudogene':
                                pass

                            else:

                                discard['stop'] += 1
                                discard_list['stop'].append(protein.id)

                                print(transcript.id, file=sys.stderr)
                                print(alt, file=sys.stderr)

                            # Truncate protein
                            alt = alt.split('*')[0]

                        if alt in variant_seen:
                            continue

                        if len(alt) > protein_length_threshold:
                            discard['too long'] += 1
                            discard_list['too long'].append(protein.id)
                            continue

                        elif len(alt) == 0:
                            discard['zero'] += 1
                            discard_list['zero'].append(protein.id)
                            continue

                        variant_seen.add(alt)

                        print(header_template.format(chrom.number, pid,
                                                     gene.name, gene.id,
                                                     transcript.id,
                                                     protein.id, alt))
                        pid += 1

                else:
                    print(
                        'Too many variants for {0}, {1}, {2}'.format(protein.id,
                                                                     transcript.id,
                                                                     protein.bin_sequence.getNbVariants(
                                                                         0)),
                        file=sys.stderr)

            bag_keys = list(bag.keys())
            for key in bag_keys:

                if any(keyword in key[0] for keyword in keywords):
                    del (bag[key])

            freeDBRegistery()

    print('Discarded protein stats:', file=sys.stderr)
    print(discard, file=sys.stderr)
    print(discard_list, file=sys.stderr)


@begin.subcommand
def split_manifest(manifest_file):
    chromosome_manifest(manifest_file)


@begin.start
def run():
    pass
