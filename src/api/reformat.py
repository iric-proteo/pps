"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Code adapted from:
https://gitlab.iric.ca/tsas/toolkit/-/blob/master/translation.py

provided and written by Céline M. Laumont and Jean-Philippe Laverdure

How to run:
python reformat.py -n <sampleName>
                   -pp <path_to_personalized_proteome_id_uniqID.fasta.txt>
                   -o <path_output_directory>

"""

# Import standard libraries
from argparse import ArgumentParser
from collections import defaultdict
import os
import re

# Third party library
from Bio.SeqIO.FastaIO import SimpleFastaParser


# ###########################################################################
# Init function
def is_valid_file(parser: ArgumentParser, p_file: str) -> str:
    """
    Verifies if file exists or exit with error

    :param parser: ArgumentParser
    :param p_file: File path
    :return: Absolute file path
    """

    p_file = os.path.abspath(p_file)

    if not os.path.exists(p_file):
        parser.error(f"The file {p_file} does not exist!")
    else:
        return p_file


def out_dir_check(parser: ArgumentParser, out_dir: str) -> str:
    """
    Verifies if path is a directory or exit with error

    :param parser: ArgumentParser
    :param out_dir: Directory path
    :return: Absolute directory path
    """

    out_dir = os.path.abspath(out_dir)

    if not os.path.isdir(out_dir):
        parser.error(f'The specified output directory {out_dir} does not '
                     f'exist!')
    else:
        return out_dir


def get_parser() -> ArgumentParser:
    """
    Reads and validate arguments
    :return: ArgumentParser instance
    """

    parser = ArgumentParser()

    parser.add_argument("-n",
                        dest="NAME",
                        help="Sample name (Default: Cancer)",
                        default='Cancer',
                        type=str)
    parser.add_argument("-pp",
                        dest="PP",
                        help="Sample's personalized proteome from MAPDP "
                             "(Default: None)",
                        default=None,
                        type=lambda x: is_valid_file(parser, x))
    parser.add_argument("-o",
                        dest="DIR_OUT",
                        help="Path to the output directory",
                        type=lambda x: out_dir_check(parser, x))

    return parser


# ###########################################################################
# Database creation functions

def pp_reformat(name: str, path_to_pp: str, path_out: str) -> str:
    """
    Reformats FASTA file

    - Add sample name and merges Ensembl identifiers to header
    - Remove duplicates

    :param name: Sample name or identifier
    :param path_to_pp: Location of the FASTA file to process
    :param path_out: Output path
    :return: New FASTA file path
    """

    pattern = re.compile('ENS[A-Z]+[0-9]+')
    protein_dic = defaultdict(list)

    with open(path_to_pp) as fasta_fh:

        for index, entry in enumerate(SimpleFastaParser(fasta_fh), start=1):
            header = entry[0]

            sub_header = f'{name}-PP_{index}_' + '_'.join(re.findall(pattern,
                                                                     header))

            protein_dic[str(entry[1])].append(sub_header)

    pp_name = os.path.basename(path_to_pp).split('.')[0]
    out_file = f'{path_out}/{pp_name}_reformat.fasta'

    with open(out_file, 'w') as o:
        for entry in protein_dic:
            o.write('>' + '|'.join(protein_dic[entry]) + '\n' + entry + '\n')

    return out_file

# ###########################################################################
# Main function


def main():
    print("\n------------------------------------------------------------------"
          "------")
    print(f"{__file__}: Reformat personalized protein database for the TSA "
          f"pipeline")
    print("--------------------------------------------------------------------"
          "----\n")

    args = get_parser().parse_args()

    if args.PP is not None and args.NAME is not None and args.DIR_OUT is not \
            None:
        personalized_proteome = pp_reformat(args.NAME, args.PP, args.DIR_OUT)
        print(f'Personalized proteome for sample {args.NAME} is here: '
              f'{personalized_proteome}')
        print('')

    else:
        print('Specify a file for the -pp, -n and -o')
        print('')


if __name__ == '__main__':
    main()
