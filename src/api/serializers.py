"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries
from django.core.files.base import ContentFile

from rest_framework import serializers

# Import project libraries
from .models import (Genome,
                     PeptidesSNPs,
                     PersonalizedProteome,
                     SNPs)
from . import tasks


class FileValidationMixin(object):
    """
    Mixin for file upload validation.
    """

    @staticmethod
    def validate_file(value):
        """
        Validates file extension and content type.
        :param value: File instance
        :return: Value or ValidationError
        """

        content_types = ['application/x-gzip', 'application/gzip',
                         'application/x-tar']

        if not (value.content_type in content_types) or \
                not value._get_name().endswith('.tar.gz'):
            message = 'File must a be a tar.gz archive.'

            raise serializers.ValidationError(message)

        else:
            return value


class GenomeSerializer(FileValidationMixin,
                       serializers.HyperlinkedModelSerializer):
    """
    Serializer for the Genome model.
    """

    import_logs = serializers.ReadOnlyField()

    import_status = serializers.ReadOnlyField()

    git_sha = serializers.ReadOnlyField(default=os.environ['GIT_SHA'])

    name = serializers.ReadOnlyField()

    species = serializers.ReadOnlyField()

    class Meta:
        fields = ('id', 'created_at', 'url', 'name', 'species', 'file',
                  'import_status', 'import_logs',
                  'trashed', 'comment', 'git_sha')

        model = Genome

    def create(self, validated_data):
        """
        Creates new instance of Genome and loads data in pyGeno database.
        :param validated_data: Dictionary
        :return: Genome instance
        """

        obj = super(GenomeSerializer, self).create(validated_data)
        obj.name = obj.file.file.name.split(os.path.sep)[-1]
        obj.save()

        tasks.import_genome.delay(obj.pk, model='genome',
                                  status_field='import_status')

        return obj


class PeptidesSNPsSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for the PeptidesSNPs model.
    """

    log_file = serializers.FileField(read_only=True)

    csv_file = serializers.FileField(read_only=True)

    bed_file = serializers.FileField(read_only=True)

    git_sha = serializers.ReadOnlyField(default=os.environ['GIT_SHA'])

    status = serializers.ReadOnlyField()

    class Meta:
        fields = (
            'id', 'created_at', 'url', 'personalized_proteome',
            'personalized_proteome_pk', 'personalized_proteome_snps_name',
            'peptides_file', 'peptides_file_name',
            'peptide_column', 'status', 'log_file', 'csv_file', 'bed_file',
            'trashed', 'comment', 'git_sha'
        )

        model = PeptidesSNPs

    def create(self, validated_data):
        """
        Creates new instance of PeptidesSNPs.
        :param validated_data: Dictionary
        :return: PeptidesSNPs instance
        """

        obj = super(PeptidesSNPsSerializer, self).create(validated_data)

        obj.log_file.save('log.txt', ContentFile(''))
        obj.csv_file.save('out.csv', ContentFile(''))
        obj.bed_file.save('out.bed', ContentFile(''))

        obj.status = 'pending'
        obj.save()

        tasks.find_peptides_snps.delay(obj.pk, model='peptidessnps',
                                       status_field='status')

        return obj

    def get_fields(self, *args, **kwargs):
        """
        Gets PersonalizedProteome instances with 'finished' status.
        :param args: Arguments
        :param kwargs: Keyword arguments
        :return: Dictionary
        """

        fields = super(PeptidesSNPsSerializer, self).get_fields(*args,
                                                                **kwargs)
        fields[
            'personalized_proteome'].queryset = PersonalizedProteome.objects.filter(
            fasta_status='finished')

        return fields


class PersonalizedProteomeSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for the PersonalizedProteome model.
    """

    quality_threshold = serializers.IntegerField(initial=20,
                                                 min_value=0)

    protein_length_threshold = serializers.IntegerField(initial=10000,
                                                        min_value=0)

    errors_file = serializers.FileField(read_only=True)

    fasta_file = serializers.FileField(read_only=True)

    fasta_status = serializers.ReadOnlyField()

    git_sha = serializers.ReadOnlyField(default=os.environ['GIT_SHA'])

    class Meta:
        fields = (
            'id', 'created_at', 'url', 'snps_id_', 'snps', 'snps_name',
            'quality_threshold',
            'expression_file', 'tpm_threshold',
            'protein_length_threshold', 'remove_redundant',
            'fasta_status', 'fasta_file', 'errors_file',
            'trashed', 'comment', 'git_sha', 'sample_id', 'reformat'
        )

        model = PersonalizedProteome

    def create(self, validated_data):
        """
        Creates new instance of PersonalizedGenome and creates FASTA file.
        :param validated_data: Dictionary
        :return: PersonalizedGenome instance
        """

        obj = super(PersonalizedProteomeSerializer, self).create(validated_data)

        reformat = ''

        if obj.reformat:
            reformat = '_reformat'

        obj.fasta_file.save(f'bs_{obj.sample_id}_personalized_proteome_id_'
                            f'{obj.pk}{reformat}.fasta.zip', ContentFile(''))
        obj.errors_file.save('errors.txt', ContentFile(''))

        obj.fasta_status = 'pending'
        obj.save()

        tasks.personalized_proteome.delay(obj.pk, model='personalizedproteome',
                                          status_field='fasta_status')

        return obj

    def get_fields(self, *args, **kwargs):
        """
        Gets SNPs instances with 'finished' import status.
        :param args: Arguments
        :param kwargs: Keyword arguments
        :return: Dictionary
        """

        fields = super(PersonalizedProteomeSerializer, self).get_fields(*args,
                                                                        **kwargs)

        fields['snps'].queryset = SNPs.objects.filter(import_status='finished')

        return fields


class SNPsSerializer(FileValidationMixin,
                     serializers.HyperlinkedModelSerializer):
    """
    Serializer for the SNPs model.
    """

    git_sha = serializers.ReadOnlyField(default=os.environ['GIT_SHA'])

    import_logs = serializers.ReadOnlyField()

    import_status = serializers.ReadOnlyField()

    name = serializers.ReadOnlyField()

    class Meta:
        fields = ('id', 'created_at', 'url', 'name', 'genome',
                  'genome_name', 'species',
                  'file', 'import_status', 'import_logs',
                  'trashed', 'comment', 'git_sha')

        model = SNPs

    def create(self, validated_data):
        """
        Creates new instance of SNPs and loads data in pyGeno database.
        :param validated_data: Dictionary
        :return: SNPs instance
        """

        obj = super(SNPsSerializer, self).create(validated_data)

        manifest = tasks.read_manifest(obj.file.path)
        obj.name = manifest['set_infos']['name']

        obj.import_status = 'finished'
        obj.save()

        return obj

    def get_fields(self, *args, **kwargs):
        """
        Gets Genome instances with 'finished' import status.
        :param args: Arguments
        :param kwargs: Keyword arguments
        :return: Dictionary
        """

        fields = super(SNPsSerializer, self).get_fields(*args, **kwargs)
        fields['genome'].queryset = Genome.objects.filter(
            import_status='finished')

        return fields
