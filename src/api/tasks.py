"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from pathlib import Path
import shutil
import time
import zipfile

# Import Django related libraries
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

# Third party libraries
from celery import (shared_task, Task)

# Import project libraries
from .manifest import read_manifest
from .models import (Genome,
                     PeptidesSNPs,
                     PersonalizedProteome)
from .nextflow import Nextflow


class CustomTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark task as failed on un-handled exception
        :param exc:
        :param task_id:
        :param args: Expects primary key at 0
        :param kwargs: Expects model and status_field
        :param einfo:
        :return: None
        """

        content_type = ContentType.objects.get(app_label='api',
                                               model=kwargs['model'])

        model_class = content_type.model_class()
        fields = {kwargs['status_field']: 'failed'}

        model_class.objects.filter(pk=args[0]).update(**fields)


def locked_transfer(genome_id: str) -> None:
    """
    Transfers pyGeno data to Nextflow temp storage with a lock.
    :param genome_id: Genome identifier
    :return: None
    """

    tmp_path = Path(settings.NEXTFLOW_FS) / 'pps' / settings.DEPLOYMENT / \
               genome_id
    tmp_path.mkdir(parents=True, exist_ok=True)
    tmp_lock = tmp_path / '_lock'
    pygeno_db = tmp_path / 'db_1.tar.gz'

    while tmp_lock.exists():
        time.sleep(2)

    if not pygeno_db.exists():

        try:
            tmp_lock.touch()
            shutil.copytree(Path(settings.MEDIA_ROOT) / 'db' / genome_id,
                            tmp_path, dirs_exist_ok=True)
            tmp_lock.unlink()

        except FileExistsError:
            while tmp_lock.exists():
                time.sleep(2)


@shared_task(base=CustomTask)
def find_peptides_snps(pk: int, model='peptidessnps',
                       status_field='status') -> None:
    """
    Finds peptide genomic location with pyGeno
    :param pk: PeptidesSNPs instance primary key
    :param model: Model class used on task failure
    :param status_field: Name of the status field used on task failure
    :return: None
    """

    obj = PeptidesSNPs.objects. \
        select_related('personalized_proteome__snps__genome').get(pk=pk)
    obj.status = 'started'
    obj.save()

    pp = obj.personalized_proteome
    snps = pp.snps

    with Nextflow(workflow='find_peptides', identifier=str(pk)) as nf:
        # Copy peptides and FASTA
        shutil.copy(obj.peptides_file.path, nf.work_dir)

        fasta_path = Path(settings.MEDIA_ROOT) / 'PersonalizedProteome' / \
                     str(pp.pk)

        if (fasta_path / 'pp.fasta').exists():
            shutil.copy(str(fasta_path / 'pp.fasta'), nf.work_dir)

        if (fasta_path / 'pp.fasta.zip').exists():

            with zipfile.ZipFile(str(fasta_path / 'pp.fasta.zip'), 'r') as \
                    zip_ref:
                zip_ref.extractall(nf.work_dir)

        # Copy SNP data wrap to work dir
        shutil.copy(snps.file.path, nf.work_dir)

        # Copy pygeno genome database
        genome_id = str(snps.genome_id)
        locked_transfer(genome_id)

        genome_path = Path(settings.NEXTFLOW_FS) / 'pps' / \
                      settings.DEPLOYMENT / genome_id
        os.symlink(genome_path, nf.work_dir / 'db')

        # Import genome to database
        params = [f'--genome_name={snps.genome.name}',
                  f'--genome_species={snps.genome.species}',
                  f'--snp_name={snps.name}',
                  f'--snp_quality_threshold={pp.quality_threshold}',
                  f'--protein_length_threshold={pp.protein_length_threshold}',
                  f'--peptide_column="{obj.peptide_column}"'
                  ]

        nf.run(params)

        dst = Path(settings.MEDIA_ROOT) / 'PeptidesSNPs' / str(obj.pk)
        shutil.copytree(nf.work_dir / 'results', dst, dirs_exist_ok=True)

    obj.status = 'finished'
    obj.save()


@shared_task(base=CustomTask)
def import_genome(pk: int, model='genome',
                  status_field='import_status') -> None:
    """
    Imports Genome into pyGeno database.

    :param pk: Genome instance primary key
    :param model: Model class used on task failure
    :param status_field: Name of the status field used on task failure

    :return: None
    """

    obj = Genome.objects.get(pk=pk)

    manifest = read_manifest(obj.file.path)

    obj.import_status = 'started'
    obj.species = manifest['genome']['species']
    obj.name = manifest['genome']['name']
    obj.save()

    genome_id = str(pk)

    with Nextflow(workflow='import_genome', identifier=genome_id) as nf:
        # Copy data wrap to work dir
        shutil.copy(obj.file.path, nf.work_dir)

        # Import genome to database
        nf.run([])

        # Store new pygeno database
        shutil.copytree(nf.work_dir / 'results',
                        Path(settings.MEDIA_ROOT) / 'db' / genome_id)

    locked_transfer(genome_id)

    obj.import_status = 'finished'
    obj.save()


@shared_task(base=CustomTask)
def personalized_proteome(pk: int, model='personalizedproteome',
                          status_field='fasta_status') -> str:
    """
    Produces a personalized proteome FASTA file using pyGeno

    :param pk: Personalized proteome instance primary key
    :param model: Model class used on task failure
    :param status_field: Name of the status field used on task failure

    :return: Personalized proteome status
    """

    obj = PersonalizedProteome.objects.select_related('snps__genome').get(pk=pk)
    obj.fasta_status = 'started'
    obj.save()

    snps = obj.snps

    with Nextflow(workflow='personalized_proteome', identifier=str(pk)) as nf:
        # Copy SNP data wrap to work dir
        shutil.copy(snps.file.path, nf.work_dir)

        # Copy pygeno genome database
        genome_id = str(snps.genome_id)
        locked_transfer(genome_id)

        genome_path = Path(settings.NEXTFLOW_FS) / 'pps' / settings.DEPLOYMENT / \
                      genome_id
        os.symlink(genome_path, nf.work_dir / 'db')

        # Import genome to database
        params = [f'--genome_name={snps.genome.name}',
                  f'--snp_name={snps.name}',
                  f'--snp_quality_threshold={obj.quality_threshold}',
                  f'--protein_length_threshold={obj.protein_length_threshold}',
                  f'--sample_id={obj.sample_id}',
                  f'--reformat={obj.reformat}',
                  f'--fasta_file=bs_{obj.sample_id}_personalized_proteome_id_'
                  f'{obj.pk}'
                  ]

        if obj.tpm_threshold is not None and obj.expression_file is not None:
            params.append(f'--tpm_threshold={obj.tpm_threshold}')
            params.append(
                f'--expression_file={Path(obj.expression_file.path).name}')
            shutil.copy(obj.expression_file.path, nf.work_dir)

        nf.run(params)

        dst = Path(settings.MEDIA_ROOT) / 'PersonalizedProteome' / str(obj.pk)
        shutil.copytree(nf.work_dir / 'results', dst, dirs_exist_ok=True)

    snps.import_status = 'finished'
    snps.save()

    obj.fasta_status = 'finished'
    obj.save()

    return obj.fasta_status
