"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.contrib.auth.models import User
from .helper import TestHelper
from rest_framework.test import APITestCase

# Third party libraries

# Import project libraries
from .helper import TestHelper


class BaseTest(TestHelper, APITestCase):
    def setUp(self):
        """
        This method setups user, group for test.
        :return: None
        """

        # Create user
        username = 'test_user'
        password = 'top_secret'

        self.user = User.objects.create_user(username=username,
                                             password=password,
                                             email='user@fake.com')

        self.client.force_authenticate(user=self.user)

        print("In method", self._testMethodName)

    def tearDown(self):
        pass
