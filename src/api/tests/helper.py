"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries
from django.conf import settings
from django.urls import reverse

# Third party libraries

# Import project libraries
from ..models import Genome, PersonalizedProteome


class TestHelper(object):
    """
    This class defines methods repetitively required by unit tests.
    """

    def create_genome(self):
        """
        Creates a Genome instance
        :return: rest_framework.response.Response
        """

        url = reverse('genome-list')

        response = None
        with open(os.path.join(settings.BASE_DIR, 'services', 'pyGeno', 'pyGeno',
                               'bootstrap_data',
                               'genomes', 'Human.GRCh37.75_Y-Only.tar.gz'),
                  'rb') as fp:
            response = self.client.post(url, {
                'filename': 'Human.GRCh37.75_Y-Only.tar.gz',
                'file': fp})

        return response

    def create_genome_bad_file_type(self):
        """
        Creates a Genome instance
        :return: rest_framework.response.Response
        """

        url = reverse('genome-list')

        response = None
        with open(os.path.join(settings.BASE_DIR, 'services', 'pyGeno',
                               'README.rst'),
                  'rb') as fp:
            response = self.client.post(url, {
                'filename': 'README.rst',
                'file': fp})

        return response

    def create_snps(self):
        """
        Creates a SNPs instance
        :return: rest_framework.response.Response
        """

        response = self.create_genome()
        Genome.objects.update(id=1, import_status='finished')

        url = reverse('snps-list')

        with open(os.path.join(settings.BASE_DIR, 'services', 'pyGeno', 'pyGeno',
                               'bootstrap_data',
                               'SNPs', 'Human.dummySRY_casava.tar.gz'),
                  'rb') as fp:
            response = self.client.post(url, {
                'genome': response.data['url'],
                'filename': 'Human.dummySRY_casava.tar.gz',
                'file': fp})

        return response

    def create_personalized_proteome(self):
        """
        Creates a Personalized proteome instance
        :param pygeno_import: Boolean
        :return: rest_framework.response.Response
        """

        response = self.create_snps()

        url = reverse('personalizedproteome-list')

        response = self.client.post(url, {
            'snps': response.data['url'],
            'quality_threshold': 20,
            'protein_length_threshold': 10000,
        })

        return response

    def create_peptides_snpsp(self):
        """
        Creates a PeptidesSNPs instance
        :return: rest_framework.response.Response
        """

        response = self.create_personalized_proteome()
        PersonalizedProteome.objects.update(id=1, fasta_status='finished')

        url = reverse('peptidessnps-list')

        with open(os.path.join(settings.BASE_DIR, 'src', 'api', 'tests',
                               'files', 'peptides.csv'),
                  'r') as fp:
            response = self.client.post(url, {
                'personalized_proteome': response.data['url'],
                'filename': 'test.csv',
                'peptides_file': fp})

        return response
