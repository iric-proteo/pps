"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from pathlib import Path
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..manifest import read_manifest, split_gtf, chromosome_manifest


class Test(BaseTest):

    def test_chromosome_manifest(self):
        os.environ['PYGENO_DATA_TMP'] = '/tmp/pygeno'

        manifest_file = Path(settings.BASE_DIR) / 'services' / 'pyGeno' / \
                        'pyGeno' / 'bootstrap_data' / 'genomes' / \
                        'Human.GRCh37.75_Y-Only.tar.gz'

        cwd = os.getcwd()
        os.chdir('/tmp')

        chromosome_manifest(str(manifest_file))

        os.chdir(cwd)

    def test_read_manifest(self):

        manifest_file = Path(settings.BASE_DIR) / 'services' / 'pyGeno' / \
                        'pyGeno' / 'bootstrap_data' / 'genomes' / \
                        'Human.GRCh37.75_Y-Only.tar.gz'

        config = read_manifest(manifest_file)
        self.assertEqual(config['genome']['species'], 'human')


