""""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import shutil
import subprocess
from unittest.mock import patch

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..nextflow import Nextflow


class Test(BaseTest):
    def setUp(self):
        super().setUp()

    def test_init(self):

        # Check that working directory is created and removed
        with Nextflow('test', 'test') as nf:

            work_dir = nf.work_dir
            self.assertTrue(work_dir.exists())

        self.assertFalse(work_dir.exists())

    def test_run(self):

        # Check that command is called
        with patch('subprocess.run') as p:
            with Nextflow('test', 'test') as nf:
                nf.run([])
                p.assert_called_once()

    def test_exception(self):
        # Directory should not be removed in case of sub process exception
        # for debugging

        def raise_called_process_error(x, check, cwd, env=''):
            raise subprocess.CalledProcessError(returncode=9, cmd='test')

        with Nextflow('test', 'test') as nf, \
                patch('subprocess.run', raise_called_process_error):
            work_dir = nf.work_dir
            self.assertTrue(work_dir.exists())

            with self.assertRaises(subprocess.CalledProcessError):
                nf.run([])

        self.assertTrue(work_dir.exists())
        shutil.rmtree(work_dir, ignore_errors=True)
