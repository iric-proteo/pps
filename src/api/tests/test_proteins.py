"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from pathlib import Path
import re
import unittest
import shutil
import tarfile
from tempfile import TemporaryDirectory
from zipfile import ZipFile

# Import Django related libraries
from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile

# Third party libraries
from Bio.SeqIO.FastaIO import SimpleFastaParser
import pandas as pd
import pyranges as pr

# Import project libraries
from .base import BaseTest
from ..models import Genome, SNPs, PersonalizedProteome
from .. import tasks
from ..manifest import read_manifest

CLEAN = os.environ.get('CLEAN', False)
LOCAL = os.environ.get('LOCAL', False)

pps_genome_path = Path(settings.BASE_DIR) / 'src/api/tests/files/genomes'
pygeno_genome_path = Path(settings.BASE_DIR) / 'services/pyGeno/pyGeno/' \
                                               'bootstrap_data/genomes'

genomes = (
    (0, ''),
    (1, pygeno_genome_path / 'Human.GRCh37.75_Y-Only.tar.gz'),
    (2, pps_genome_path / 'GRCh38_99.tar.gz'),
    (3, pps_genome_path / 'GRCh38_99_chromosome_14.tar.gz'),
)

pps_snps_path = Path(settings.BASE_DIR) / 'src/api/tests/files/SNPs'

snps = {
    'Empty': pps_snps_path / 'Empty.tar.gz',
    'Homozygote': pps_snps_path / 'Homozygote.tar.gz',
    'Heterozygote_ref_var': pps_snps_path / 'Heterozygote_ref_var.tar.gz',
    'Heterozygote_var_var': pps_snps_path / 'Heterozygote_var_var.tar.gz',
    'Quality': pps_snps_path / 'Quality.tar.gz',
}


@unittest.skipIf(LOCAL is False, 'LOCAL is False')
class Test(BaseTest):

    def setUp(self):

        if CLEAN:
            # Cleans every files
            shutil.rmtree(Path(settings.MEDIA_ROOT), ignore_errors=True)
            shutil.rmtree(Path(settings.NEXTFLOW_FS), ignore_errors=True)
        else:
            # Don't removes genomes
            shutil.rmtree(Path(settings.MEDIA_ROOT) / 'SNPs',
                          ignore_errors=True)
            shutil.rmtree(Path(settings.MEDIA_ROOT) / 'PersonalizedProteome',
                          ignore_errors=True)
            shutil.rmtree(Path(settings.NEXTFLOW_FS) / 'personalized_proteome',
                          ignore_errors=True)

    @staticmethod
    def gtf_extract_protein_identifiers(manifest_file: Path,
                                        genome_path: Path) -> None:
        """
        Extracts protein identifiers from GTF file

        :param manifest_file: Manifest file path to get the GTF file
        :param genome_path: Genome directory
        :return: Write protein identifiers in genome path
        """

        manifest = read_manifest(str(manifest_file))
        gtf_location = manifest['gene_set']['gtf']

        os.environ['PYGENO_DATA_TMP'] = '/tmp'
        from pyGeno.importation.Genomes import _getFile
        gtf_file = _getFile(gtf_location, '.')

        if not gtf_location.startswith('ftp') and \
                not gtf_location.startswith('http'):
            with tarfile.open(manifest_file, mode='r:gz') as fh_archive:
                fh_archive.extract(gtf_file)

        gr = pr.read_gtf(gtf_file)

        # Discard proteins from scaffold
        tmp_df = gr.df[['Chromosome', 'protein_id']]
        tmp_df['chromosome_str_len'] = tmp_df['Chromosome'].astype(str).apply(
            len)
        tmp_df = tmp_df[tmp_df['chromosome_str_len'] < 3]

        protein_identifiers = tmp_df['protein_id'].dropna().drop_duplicates()
        protein_identifiers.to_csv(genome_path / 'gtf_protein_identifier.csv',
                                   index=False)
        Path(gtf_file).unlink()

    @staticmethod
    def read_gtf_identifiers(genome_id: int) -> set:
        """
        Reads Ensembl protein identifiers extracted from GTF

        :param genome_id: Genome identifier
        :return: Protein identifiers
        """

        genome_path = Path(settings.MEDIA_ROOT) / 'db' / str(genome_id)
        gtf_csv = genome_path / 'gtf_protein_identifier.csv'

        return set(pd.read_csv(gtf_csv)['protein_id'])

    @staticmethod
    def import_genome(genome_id: int):
        """
        Import genome if it doesn't exist.

        :param genome_id: Genome identifier
        :return: None
        """

        manifest_file = genomes[genome_id][1]
        manifest = read_manifest(manifest_file)

        genome_path = Path(settings.MEDIA_ROOT) / 'db' / str(genome_id)

        if not genome_path.exists():

            with manifest_file.open('rb') as fh:
                genome = Genome.objects.create(
                    pk=genome_id, file=File(fh, name=manifest_file.name))

            tasks.import_genome(genome_id)
            Test.gtf_extract_protein_identifiers(manifest_file, genome_path)

        else:
            genome = Genome.objects.create(pk=genome_id,
                                           name=manifest['genome']['name'])

        return genome

    @staticmethod
    def create_snp(genome: Genome, manifest_name: str) -> SNPs:
        """
        Creates SNP instance

        :param genome: Genome instance
        :param manifest_name: SNP manifest to use
        :return:
        """

        manifest_file = snps[manifest_name]

        manifest = read_manifest(manifest_file)

        with manifest_file.open('rb') as fh:
            snp = SNPs.objects.create(genome=genome,
                                      file=File(fh, name=manifest_file.name),
                                      name=manifest['set_infos']['name'])

        return snp

    @staticmethod
    def read_protein_identifiers_from_fasta(pp: PersonalizedProteome) -> set:
        """
        Extract Ensembl protein identifiers from the FASTA file

        :param pp: Personalized proteome instance
        :return: Protein identifiers
        """

        identifiers = set()

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            pattern = re.compile('ENSP+[0-9]+')

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:
                for record in SimpleFastaParser(fasta_fh):
                    identifiers.update(re.findall(pattern,
                                                  record[0]))

        return identifiers

    @staticmethod
    def fasta_stats(pp: PersonalizedProteome) -> tuple:
        """
        Extracts protein and residues count from FASTA files

        :param pp: Personalized proteome instance
        :return: Protein and residues count
        """

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:

                protein_counts = 0
                residues_counts = 0

                for record in SimpleFastaParser(fasta_fh):
                    protein_counts += 1
                    residues_counts += len(record[1])

        return protein_counts, residues_counts

    @staticmethod
    def read_discarded_protein_identifiers(pp: PersonalizedProteome) -> set:
        """"
        Reads discarded protein from log files

        Proteins are discarded because they are too long or zero length

        :param pp: Personalized proteome instance
        :return: Protein identifiers
        """

        identifiers = set()

        for file in Path(pp.fasta_file.path).parent.glob('*.txt'):

            with file.open() as fh_in:

                for line in fh_in:
                    if line.startswith("defaultdict(<class 'list'>, "):
                        line = line.rstrip()
                        discarded = eval(line.split("<class 'list'>, ")[1]
                                         .rstrip(')'))

                        for key in ['zero', 'too long']:
                            if key in discarded:
                                identifiers.update(set(discarded[key]))

        return identifiers

    @staticmethod
    def generate_personalized_proteome(genome_id: int, snp_name: str,
                                       protein_length: int = 10000,
                                       expression_file: str = None,
                                       tpm_threshold=0) \
            -> PersonalizedProteome:
        """
        Generates personalized proteome

        :param genome_id: Genome identifier
        :param snp_name: SNP set name
        :param protein_length: Protein length threshold
        :param expression_file: Transcript expression file from Kalisto
        :param tpm_threshold: Transcript expression threshold in TPM
        :return: Personalized proteome instance
        """

        genome = Test.import_genome(genome_id)

        snp = Test.create_snp(genome, snp_name)

        pp = PersonalizedProteome.objects.create(
            protein_length_threshold=protein_length, snps=snp,
            sample_id='Test', reformat=True,
        )
        pp.fasta_file.save(f'bs_{pp.sample_id}_personalized_proteome_id_'
                  f'{pp.pk}_reformat.fasta.zip', ContentFile(''))

        if expression_file is not None:
            expression_file = expression_file

            with expression_file.open('rb') as fh:
                pp.expression_file = File(fh, name=expression_file.name)
                pp.tpm_threshold = tpm_threshold
                pp.save()

        tasks.personalized_proteome(pp.pk)

        return pp

    def test_chromosome_Y_proteome(self):
        """
        Checks that all proteins from chromosome Y are found in the FASTA
        file output

        GTF is within the data wrap
        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Empty'

        pp = Test.generate_personalized_proteome(genome_id, snp_name)

        fasta_identifiers = self.read_protein_identifiers_from_fasta(pp)
        gtf_identifiers = self.read_gtf_identifiers(genome_id)

        # Check equals number of identifiers
        self.assertEqual(len(fasta_identifiers), len(gtf_identifiers))

        # Check identical identifiers
        self.assertEqual(len(gtf_identifiers - fasta_identifiers), 0)

    def test_chromosome_Y_proteome_length(self):
        """
        Checks that protein length filter discard proteins

        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Empty'

        pp = Test.generate_personalized_proteome(genome_id, snp_name,
                                                 protein_length=1)

        fasta_identifiers = self.read_protein_identifiers_from_fasta(pp)
        discarded_identifiers = Test.read_discarded_protein_identifiers(pp)

        # Check that all protein are discarded
        self.assertEqual(len(fasta_identifiers), 0)
        self.assertEqual(len(discarded_identifiers), 160)

    def test_chromosome_Y_proteome_transcript_expression_filtered(self):
        """
        Checks that only proteins with transcript expression are found in the
        FASTA file

        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Empty'
        expression_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                            'expression_chromosome_Y.tsv'
        tpm_threshold = 0.5

        pp = Test.generate_personalized_proteome(
            genome_id, snp_name, expression_file=expression_file,
            tpm_threshold=tpm_threshold
        )

        fasta_identifiers = self.read_protein_identifiers_from_fasta(pp)

        # Check expected number of identifiers
        self.assertEqual(len(fasta_identifiers), 2)

        # Check expected identifiers
        self.assertTrue('ENST00000414629' not in fasta_identifiers and
                        'ENSP00000330446' in fasta_identifiers and
                        'ENSP00000371844' in fasta_identifiers)

    def test_full_proteome(self):
        """
        Checks that all proteins are found in the FASTA file output

        GTF is remote

        Requires about 5GB of RAM for GTF processing
        """

        genome_id = 2  # GRCh38_99.tar.gz
        snp_name = 'Empty'

        pp = Test.generate_personalized_proteome(genome_id, snp_name)

        discarded_identifiers = Test.read_discarded_protein_identifiers(pp)
        fasta_identifiers = self.read_protein_identifiers_from_fasta(pp)
        gtf_identifiers = self.read_gtf_identifiers(genome_id)

        # Check equals number of identifiers
        self.assertEqual(len(fasta_identifiers) + len(discarded_identifiers),
                         len(gtf_identifiers))

        # Check identical identifiers
        self.assertEqual(len(gtf_identifiers - fasta_identifiers
                             - discarded_identifiers), 0)

        print(Test.fasta_stats(pp))

    def test_homozygote_snp(self):
        """
        Checks that homozygote SNP is correctly processed
        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Homozygote'
        expression_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                            'expression_chromosome_Y_single.tsv'
        tpm_threshold = 0.5

        pp = Test.generate_personalized_proteome(
            genome_id, snp_name, expression_file=expression_file,
            tpm_threshold=tpm_threshold
        )

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:

                line_count = 0
                line = ''

                for line in fasta_fh:
                    line_count += 1

                self.assertEqual(line[0], 'C')
                self.assertEqual(line_count, 2)

    def test_heterozygote_snp_ref_var(self):
        """
        Checks that heterozygote SNP are correctly processed
        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Heterozygote_ref_var'
        expression_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                            'expression_chromosome_Y_single.tsv'
        tpm_threshold = 0.5

        pp = Test.generate_personalized_proteome(
            genome_id, snp_name, expression_file=expression_file,
            tpm_threshold=tpm_threshold
        )

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:

                line_count = 0

                for line in fasta_fh:
                    line_count += 1

                    if line_count == 2:
                        self.assertEqual(line[0], 'C')

                    if line_count == 4:
                        self.assertEqual(line[0], 'R')

                self.assertEqual(line_count, 4)

    def test_heterozygote_snp_var_var(self):
        """
        Checks that heterozygote SNP are correctly processed
        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Heterozygote_var_var'
        expression_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                            'expression_chromosome_Y_single.tsv'
        tpm_threshold = 0.5

        pp = Test.generate_personalized_proteome(
            genome_id, snp_name, expression_file=expression_file,
            tpm_threshold=tpm_threshold
        )

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:

                line_count = 0

                for line in fasta_fh:
                    line_count += 1

                    if line_count == 2:
                        self.assertEqual(line[0], 'C')

                    if line_count == 4:
                        self.assertEqual(line[0], 'S')

                self.assertEqual(line_count, 4)

    def test_quality_discarded_snp(self):
        """
        Checks that low quality SNP SNP is discarded
        """

        genome_id = 1  # Human.GRCh37.75_Y-Only
        snp_name = 'Quality'
        expression_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                            'expression_chromosome_Y_single.tsv'
        tpm_threshold = 0.5

        pp = Test.generate_personalized_proteome(
            genome_id, snp_name, expression_file=expression_file,
            tpm_threshold=tpm_threshold
        )

        with TemporaryDirectory() as tmp_directory, \
                ZipFile(pp.fasta_file, 'r') as zipObj:
            zipObj.extractall(tmp_directory)

            fasta_file = f'bs_{pp.sample_id}_personalized_proteome_id_{pp.pk}' \
                         f'_reformat.fasta'

            with open(Path(tmp_directory) / fasta_file, 'r') as fasta_fh:

                line_count = 0
                line = ''

                for line in fasta_fh:
                    line_count += 1

                self.assertEqual(line[0], 'R')
                self.assertEqual(line_count, 2)
