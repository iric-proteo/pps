"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import contextlib
import io
from pathlib import Path
import sys

# Import Django related libraries
from django.conf import settings

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..reformat import main


class Test(BaseTest):

    def setUp(self):
        super().setUp()

    def test_main_no_params(self):
        """
        Check running with out parameters prints warning
        """

        sys.argv = ['reformat.py']

        stdout_output = io.StringIO()

        with contextlib.redirect_stdout(stdout_output):
            main()

        self.assertTrue('Specify' in stdout_output.getvalue())

    def test_is_valid_file_missing(self):
        """
        Checks file FASTA validation, missing file should trigger error msg
        and exit
        """

        sys.argv = ['reformat.py', '-pp', 'fake']

        stderr_output = io.StringIO()

        with contextlib.redirect_stderr(stderr_output), \
             self.assertRaises(SystemExit):
            main()

        self.assertTrue('not exist' in stderr_output.getvalue())

    def test_is_valid_file_exists(self):
        """
        Checks file FASTA validation
        """

        sys.argv = ['reformat.py', '-pp', '.']

        stderr_output = io.StringIO()

        with contextlib.redirect_stderr(stderr_output):
            main()

        self.assertFalse('not exist' in stderr_output.getvalue())

    def test_out_dir_check_missing(self):
        """
        Checks directory validation, missing should trigger error msg
        and exit
        """

        sys.argv = ['reformat.py', '-o', 'fake']

        stderr_output = io.StringIO()

        with contextlib.redirect_stderr(stderr_output), \
             self.assertRaises(SystemExit):
            main()

        self.assertTrue('not exist' in stderr_output.getvalue())

    def test_out_dir_check(self):
        """
        Checks directory validation
        """

        sys.argv = ['reformat.py', '-o', '.']

        stderr_output = io.StringIO()

        with contextlib.redirect_stderr(stderr_output):
            main()

        self.assertFalse('not exist' in stderr_output.getvalue())

    def test_pp_reformat(self):
        """
        Reformat should merge header and removes duplicates entries
        """

        fasta_file = Path(settings.BASE_DIR) / 'src/api/tests/files/' \
                                               'for_reformat.fasta'

        sys.argv = ['reformat.py', '-pp', str(fasta_file), '-o', '/tmp']

        main()

        out_file = Path('/tmp/for_reformat_reformat.fasta')

        with out_file.open('r') as fh:

            line = fh.readline().rstrip()
            header = '>Cancer-PP_1_ENSG00000186092_ENST00000641515_ENSP00000493' \
                     '376|Cancer-PP_2_ENSG00000186092_ENST00000641515_ENSP00000' \
                     '493376'
            self.assertEqual(line, header)

            line_count = 1

            for _ in fh:
                line_count += 1

            self.assertEqual(line_count, 2)

        out_file.unlink()
