"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.core.files import File
from django.core.files.uploadedfile import UploadedFile

from rest_framework import serializers

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..serializers import FileValidationMixin


class SerializersTest(BaseTest):
    def test_validate_file(self):

        with open('manage.py', 'r') as fp, self.assertRaises(serializers.ValidationError):

            file = File(fp)
            ufile = UploadedFile(file=file, content_type='fake')

            FileValidationMixin.validate_file(ufile)
