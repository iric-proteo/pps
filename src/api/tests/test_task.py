"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from pathlib import Path
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings
from django.core.files import File

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..models import Genome, SNPs, PersonalizedProteome, PeptidesSNPs
from .. import tasks


class Test(BaseTest):

    def test_CustomTask_on_failure(self):
        obj = Genome.objects.create()

        kwargs = {'model': 'genome', 'status_field': 'import_status'}
        tasks.CustomTask.on_failure(1, 2, 3, [1], kwargs, 6)

        obj.refresh_from_db()

        self.assertEqual(obj.import_status, 'failed')

    def test_import_genome(self):

        manifest_file = Path(settings.BASE_DIR) / 'services' / 'pyGeno' / \
                             'pyGeno' / 'bootstrap_data' / 'genomes' / \
                             'Human.GRCh37.75_Y-Only.tar.gz'

        with manifest_file.open('rb') as fh:
            genome = Genome.objects.create(file=File(fh, name=manifest_file.name))

        with patch('api.nextflow.Nextflow.run') as p1, \
                patch('shutil.copytree') as p2:

            tasks.import_genome(1)

            p1.assert_called_once()
            p2.assert_called()

        genome.refresh_from_db()

        self.assertEqual(genome.import_status, 'finished')
        self.assertEquals(genome.name, 'GRCh37.75_Y-Only')
        self.assertEquals(genome.species, 'human')

    def test_personalized_proteome(self):
        genome = Genome.objects.create()

        manifest_file = Path(settings.BASE_DIR) / 'services' / 'pyGeno' / \
                        'pyGeno' / 'bootstrap_data' / 'SNPs' / \
                        'Human_agnostic.dummySRY.tar.gz'

        with manifest_file.open('rb') as fh:
            snp = SNPs.objects.create(genome=genome,
                                      file=File(fh, name=manifest_file.name))

        # This fake file is not in the right format
        with manifest_file.open('rb') as fh:
            pp = PersonalizedProteome.objects.create(
                snps=snp,
                tpm_threshold=0,
                expression_file=File(fh, name=manifest_file.name))

        with patch('api.nextflow.Nextflow.run') as p1, \
                patch('shutil.copytree') as p2:

            tasks.personalized_proteome(1)

            p1.assert_called_once()
            p2.assert_called()

        pp.refresh_from_db()
        self.assertEqual(pp.fasta_status, 'finished')

    def test_find_peptides_snps(self):

        genome = Genome.objects.create()

        manifest_file = Path(settings.BASE_DIR) / 'services' / 'pyGeno' / \
                        'pyGeno' / 'bootstrap_data' / 'SNPs' / \
                        'Human_agnostic.dummySRY.tar.gz'

        with manifest_file.open('rb') as fh:
            snp = SNPs.objects.create(genome=genome,
                                      file=File(fh, name=manifest_file.name))

            # This fake file is not in the right format
            pp = PersonalizedProteome.objects.create(
                snps=snp,
                tpm_threshold=0,
                expression_file=File(fh, name=manifest_file.name),
                fasta_file=File(fh, name='fasta.fa')
            )

            # This fake file is not in the right format
            psnps = PeptidesSNPs.objects.create(
                personalized_proteome=pp,
                peptides_file=File(fh, name='peptides.csv'),
            )

        with patch('api.nextflow.Nextflow.run') as p1, \
                patch('shutil.copytree') as p2:

            tasks.find_peptides_snps(1)

            p1.assert_called_once()
            p2.assert_called()

        psnps.refresh_from_db()
        self.assertEqual(psnps.status, 'finished')


