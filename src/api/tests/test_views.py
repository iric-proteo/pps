"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from unittest.mock import patch


# Import Django related libraries
from django.urls import reverse

from rest_framework import status

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..models import PersonalizedProteome


class ViewsTest(BaseTest):
    def test_root(self):
        """
        Ensures that expected resources are available.
        """

        url = '/'
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('genomes' in response.data)
        self.assertTrue('personalized_proteomes' in response.data)
        self.assertTrue('snps' in response.data)

    def test_genome_auth(self):
        """
        Ensures genome resource authentication
        """

        self.client.force_authenticate(user=None)

        url = reverse('genome-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_genome_list_empty(self):
        """
        Ensure that genome empty list is working
        """

        url = reverse('genome-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEquals(len(response.data), 0)

    def test_genome_create(self):
        """
        Ensures genome creation
        """

        with patch('api.tasks.import_genome.delay') as p:

            response = self.create_genome()

            p.assert_called_once()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('genome-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(response.data), 1)

        # Check file validation
        response = self.create_genome_bad_file_type()
        self.assertTrue('File must a be a tar.gz archive' in
                        str(response.content))

    def test_genome_delete(self):
        """
        Ensures genome deletion
        """

        with patch('api.tasks.import_genome.delay') as p:

            response = self.create_genome()

            p.assert_called_once()

        url = response.data['url']

        response = self.client.delete(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_snps_auth(self):
        """
        Ensures SNPs resource authentication
        """

        self.client.force_authenticate(user=None)

        url = reverse('snps-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_snps_list_empty(self):
        """
        Ensure that snps empty list is working
        """

        url = reverse('snps-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEquals(len(response.data), 0)

    def test_snps_create(self):
        """
        Ensures snps creation
        """

        with patch('api.tasks.import_genome.delay') as p:

            response = self.create_snps()

            p.assert_called_once()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('snps-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(response.data), 1)

    def test_personalized_proteome_auth(self):
        """
        Ensures Personalized proteome resource authentication
        """

        self.client.force_authenticate(user=None)

        url = reverse('personalizedproteome-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_personalized_proteome_list_empty(self):
        """
        Ensure that Personalized proteome empty list is working
        """

        url = reverse('personalizedproteome-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEquals(len(response.data), 0)

    def test_personalized_proteome_create(self):
        """
        Ensures Personalized proteome creation
        """

        with patch('api.tasks.import_genome.delay') as p1, \
                patch('api.tasks.personalized_proteome.delay') as p2:

            response = self.create_personalized_proteome()
            p1.assert_called_once()
            p2.assert_called_once()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        str(PersonalizedProteome.objects.get(id=1))

        url = reverse('personalizedproteome-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(response.data), 1)

    def test_peptidessnps_auth(self):
        """
        Ensures PeptidesSNPs resource authentication
        """

        self.client.force_authenticate(user=None)

        url = reverse('peptidessnps-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_peptidessnps_list_empty(self):
        """
        Ensure that PeptidesSNPs empty list is working
        """

        url = reverse('peptidessnps-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEquals(len(response.data), 0)

    def test_peptidessnps_create(self):
        """
        Ensures PeptidesSNPs creation
        """

        with patch('api.tasks.import_genome.delay') as p1, \
                patch('api.tasks.personalized_proteome.delay') as p2, \
                patch('api.tasks.find_peptides_snps.delay') as p3:

            response = self.create_peptides_snpsp()
            p1.assert_called_once()
            p2.assert_called_once()
            p3.assert_called_once()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('peptidessnps-list')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(response.data), 1)
