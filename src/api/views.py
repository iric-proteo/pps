"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from rest_framework import (mixins,
                            permissions,
                            viewsets)

# Import project libraries
from .models import (Genome,
                     PeptidesSNPs,
                     PersonalizedProteome,
                     SNPs)
from .serializers import (GenomeSerializer,
                          PeptidesSNPsSerializer,
                          PersonalizedProteomeSerializer,
                          SNPsSerializer)


class CustomViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.ListModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    """
    Custom ViewSet to remove update.
    """

    pass


class DestroyMixin(object):
    def perform_destroy(self, instance):
        """
        Removes instance and removes data wrap from pyGeno database
        :param instance: Genome or SNPs
        :return: None
        """

        instance.trashed = True
        instance.save()


class MultipleMixin(object):

    def get_queryset(self):

        # Get URL parameter as a string, if exists
        ids = self.request.query_params.get('ids', None)
        queryset = self.queryset

        # Get snippets for ids if they exist
        if ids is not None:
            # Convert parameter string to list of integers
            ids = [int(x) for x in ids.split(',')]
            # Get objects for all parameter ids
            queryset = self.queryset.filter(pk__in=ids)

        return queryset


class GenomeViewSet(MultipleMixin, DestroyMixin, CustomViewSet):
    """
    This endpoint presents the Genome instances in the system.
    """

    filter_fields = ('import_status', 'trashed',)

    permission_classes = (permissions.IsAuthenticated,)

    queryset = Genome.objects.all()

    serializer_class = GenomeSerializer


class PeptidesSNPsViewSet(CustomViewSet):
    """
    This endpoint presents the PeptidesSNPs instances in the system.
    """

    filter_fields = ('status', 'trashed',)

    permission_classes = (permissions.IsAuthenticated,)

    queryset = PeptidesSNPs.objects.all()

    serializer_class = PeptidesSNPsSerializer


class PersonalizedProteomeViewSet(MultipleMixin, CustomViewSet):
    """
    This endpoint presents the Personalized Proteome instances in the system.
    """

    filter_fields = ('fasta_status', 'trashed',)

    permission_classes = (permissions.IsAuthenticated,)

    queryset = PersonalizedProteome.objects.select_related('snps__genome')

    serializer_class = PersonalizedProteomeSerializer


class SnpsViewSet(MultipleMixin, DestroyMixin, CustomViewSet):
    """
    This endpoint presents the SNPs instances in the system.
    """

    filter_fields = ('import_status', 'trashed',)

    permission_classes = (permissions.IsAuthenticated,)

    queryset = SNPs.objects.select_related('genome')

    serializer_class = SNPsSerializer
