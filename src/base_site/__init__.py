"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries
from __future__ import absolute_import, unicode_literals

# Import Django related libraries

# Third party libraries


# Import project libraries
from .celery import app as celery_app


# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.

__all__ = ['celery_app']
