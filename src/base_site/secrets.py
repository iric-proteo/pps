"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries

# Import project libraries


def get_secret(name):
    """
    Gets secret from file or environment variable
    :param name: String
    :return: String
    """

    # Look first in file for secret
    path = os.environ.get('SECRET_PATH', '')

    file_ = os.path.join(path, name)

    if os.path.exists(file_):

        with open(file_, 'r') as f:

            return f.readline().strip()

    return os.environ[name]



