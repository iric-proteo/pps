"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries

# Import third party libraries
from kombu.common import Exchange, Queue

# Import project libraries
from .base import *
from ..secrets import get_secret

# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '*').split()

ADMINS = (
    (os.environ.get('ADMIN_NAME', ''), os.environ.get('ADMIN_EMAIL', '')),
)

DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': os.environ.get('POSTGRES_HOST', ''),
        'NAME': os.environ.get('POSTGRES_DB', ''),
        'USER': os.environ.get('POSTGRES_USER', ''),
        'PASSWORD': get_secret('POSTGRES_PASSWORD'),
        'CONN_MAX_AGE': 0,
    },
}

MANAGERS = (
    (os.environ.get('ADMIN_NAME', ''), os.environ.get('ADMIN_EMAIL', '')),
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret('SECRET_KEY')

# Mail server configuration
EMAIL_USE_TLS = True
EMAIL_HOST = os.environ.get('EMAIL_HOST', '')
EMAIL_USER = os.environ.get('EMAIL_USER', '')
EMAIL_PASSWORD = get_secret('EMAIL_PASSWORD')
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = '[PPS] '

TIME_ZONE = os.environ.get('TIME_ZONE', '')
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
    'formatters': {
        'json': {
            '()': 'pythonjsonlogger.jsonlogger.JsonFormatter',
            'format': '%(asctime) %(name) %(processName) %(filename) %(funcName)'
                      ' %(levelname) %(lineno) %(module) %(threadName) %(message)',
        },
    },
}

REDIS = os.environ.get('REDIS_HOST', 'redis://redis:6379')
CELERY_TASK_ACKS_LATE = True
CELERY_BROKER_URL = REDIS + '/0'
CELERY_RESULT_BACKEND = REDIS + '/1'
CELERY_TIMEZONE = TIME_ZONE
CELERY_WORKER_MAX_TASKS_PER_CHILD = 1

PYGENO_DATA = '/var/www'

# Elasticsearch APM
if os.environ.get('ELASTIC_APM_HOST', False):
    import urllib3
    urllib3.disable_warnings()
    ELASTIC_APM = {
        'CLOUD_PROVIDER': "none",
        'SERVICE_NAME': 'pps',
        'SERVER_URL': os.environ.get('ELASTIC_APM_HOST'),
        'SECRET_TOKEN': get_secret('ELASTIC_APM_TOKEN'),
        'VERIFY_SERVER_CERT': False,
    }
    INSTALLED_APPS += ['elasticapm.contrib.django']

    MIDDLEWARE.insert(1, 'elasticapm.contrib.django.middleware.Catch404Middleware')
    MIDDLEWARE.insert(0, 'elasticapm.contrib.django.middleware.TracingMiddleware')

# Nextflow
NEXTFLOW_FS = '/mnt/nextflow'
NEXTFLOW_BIN_PATH = '/usr/app/bin'
NEXTFLOW_PROFILE = 'k8s'
NEXTFLOW_WORKFLOW_PATH = '/usr/app/src'


DEPLOYMENT = os.environ.get('DEPLOYMENT')
