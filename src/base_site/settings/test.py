"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

print('Loading test settings...\n')

# Import standard libraries

# Import Django related libraries

# Import project libraries
from .base import *

DEBUG = True

# BROKER_BACKEND = 'memory://'

# TEST_RUNNER = 'djcelery.contrib.test_runner.CeleryTestSuiteRunner'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db', 'db_test.sqlite3'),

        # for sqlite write lock timeout
        'OPTIONS': {
            'timeout': 30,
        },
    }
}

CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True
PYGENO_DATA = '/var/www'

os.environ['GIT_SHA'] = 'local'
os.environ['TAG'] = 'build'

MEDIA_ROOT = '/tmp/pps_test_local/media_root'

NEXTFLOW_FS = '/tmp/nextflow'
NEXTFLOW_BIN_PATH = '/home/mathieu'  # Replace with os env
NEXTFLOW_PROFILE = 'singularity'
NEXTFLOW_WORKFLOW_PATH = '/home/mathieu/code/pps/src'  # Replace with os env
NXF_SINGULARITY_CACHEDIR = '/home/mathieu/singularity_img'  # Replace with os env

DEPLOYMENT = 'test'