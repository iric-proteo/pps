"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from rest_framework.decorators import api_view
from django.http import HttpResponse


# Third party libraries

# Import project libraries

def alive(request):
    """
    Returns HttpResponse with status 204
    :param request: HttpRequest
    :return: HttpResponse
    """

    return HttpResponse(status=204)



@api_view()
def server_secure_media(request, url):
    response = HttpResponse()
    response["Content-Disposition"] = 'attachment; filename=%s' % \
                                      url.split('/')[-1]
    response['X-Accel-Redirect'] = '/protected_media/' + url

    return response
