#!/bin/bash

_term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$child" 2>/dev/null
}

trap _term SIGTERM

python manage.py collectstatic --noinput --settings=base_site.settings.base

sleep 15

python manage.py migrate

while [ $? -ne 0 ]; do
    sleep 15
    python manage.py migrate
done

FILE=/usr/app/media_root/pps
if [ ! -f "$FILE" ]; then
    touch /usr/app/media_root/pps
    touch /var/www/pps
    python manage.py init_pps
fi

gunicorn -c ../services/gunicorn/gunicorn_conf.py base_site.wsgi:application &

child=$!
wait "$child"