#!/usr/bin/env nextflow

/*
========================================================================================
                 Find peptide workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
* Import SNP set
* Find peptides for each chromosome
* Merge bed and csv


Notes:
This workflow expect a pygeno database in the current working directory with
a pre-loaded genome.

----------------------------------------------------------------------------------------

How to run:

Singlularity
* TAG=build ~/nextflow ~/code/pps/src/nf-workflows/find_peptides/main.nf -profile singularity -resume --genome_name="GRCh38.88 v2" --genome_species="human" --snp_name="1_0_SNPSet" --peptide_column="peptide_sequence" --snp_quality_threshold=20

----------------------------------------------------------------------------------------
*/


/*
 * Define the default parameters
 */

snp_packages = Channel.fromPath('*.tar.gz')
pygeno_data = Channel.fromPath('db/*.tar.gz')

peptides = Channel.fromPath('*.csv')
fasta_files = Channel.fromPath('pp.fasta')

/*
 * Check empty user provided parameters
 */

if(params.genome_name == null) {
    exit 1, "ERROR: genome_name is empty"
}

if(params.genome_species == null) {
    exit 1, "ERROR: genome_species is empty"
}

if(params.snp_name == null) {
    exit 1, "ERROR: snp_name is empty"
}


if(params.peptide_column == null) {
    exit 1, "ERROR: peptide_column is empty"
}


if(params.snp_quality_threshold == null) {
    exit 1, "ERROR: snp_quality_threshold is empty"
}



log.info """\
=====================================
 Find peptides Workflow
=====================================
 Genome name: ${params.genome_name}
 Genome species: ${params.genome_species}
 SNP name: ${params.snp_name}
 SNP quality threshold: ${params.snp_quality_threshold}
 Peptide column: ${params.peptide_column}
=====================================
"""

/**
 Search peptide location for each chromosome
 */
process find_peptides {
    container "$PPS_CONTAINER"
    memory = '400MB'

    scratch true

    errorStrategy 'retry'

    input:
    each file(peptide_file) from peptides
    each file(fasta_file) from fasta_files
    file db_file from pygeno_data
    each file(snp_package) from snp_packages

    output:
    file 'matches*.csv' optional true into matches_csv_files
    file 'matches*.bed' optional true into matches_bed_files

    script:
    """

    chromosome=`echo $db_file | sed "s/db_//" | sed "s/.tar.gz//"`
    echo \$chromosome

    tar xvzf ${db_file}

    PYGENO_DATA_TMP=. python /usr/app/src/api/pygeno.py import_snps ${snp_package} \$chromosome 2>&1 | tee import_snps_\$chromosome.log

    PYGENO_DATA_TMP=. python /usr/app/src/api/findPeptidesOverlappingSNPs_transcriptome_pG2.py \
    -g "${params.genome_name}" \
    -s "${params.genome_species}" \
    -c \$chromosome \
    -d "${params.snp_name}" \
    -p ${params.peptide_column} \
    -t ${params.snp_quality_threshold} \
    pp.fasta \
    ${peptide_file} \
    matches_\$chromosome.csv

    """

}


matches_csv_files.collectFile(keepHeader: true, skip: 1, name: 'out.csv', storeDir: "${params.outdir}")
matches_bed_files.collectFile(keepHeader: true, skip: 1, name: 'out.bed', storeDir: "${params.outdir}")

workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}