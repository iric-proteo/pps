#!/usr/bin/env nextflow


/*
========================================================================================
                 Import genome Workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
* Split genome data wrap per chromosome
* Import genome into pyGeno database
----------------------------------------------------------------------------------------

How to run:
* TAG=build ~/nextflow ~/code/pps/src/nf-workflows/import_genome/main.nf  -profile singularity -resume
----------------------------------------------------------------------------------------
*/


/*
 * Define the default parameters
 */

genome_packages = Channel.fromPath('*.tar.gz')



log.info """\
=====================================
  Import genome Workflow
=====================================
"""

/**
Split pygeno genome data wrap
*/
process split_manifest {

    container "$PPS_CONTAINER"
    memory = '100MB'

    scratch true

    input:
    file 'genome_data_wrap.tar.gz' from genome_packages

    output:
    file 'manifest_*.tar.gz' into chromosome_packages

    script:
    """
    PYGENO_DATA_TMP=. python  /usr/app/src/api/pygeno.py split_manifest genome_data_wrap.tar.gz
    """
}

/**
Import genome into pyGeno database
*/
process import_genome {

    container "$PPS_CONTAINER"
    memory = '1000MB'

    scratch true

    errorStrategy 'retry'

    input:
    file chromosome_package from chromosome_packages.flatten()

    output:
    file 'db_*'

    publishDir "${params.outdir}", mode: 'move'

    script:
    """
    chromosome=`echo "${chromosome_package}" | sed 's/.tar.gz//' | sed 's/manifest_//'`

    PYGENO_DATA_TMP=. python /usr/app/src/api/pygeno.py import_genome ${chromosome_package} 2>&1 | tee import_genome.log

    tar -czvf db_\$chromosome.tar.gz data pyGenoRaba.db

    """
}


workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}