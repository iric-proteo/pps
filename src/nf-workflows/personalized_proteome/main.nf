#!/usr/bin/env nextflow

/*
========================================================================================
                 Personalize proteome workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
* Import SNP set
* Generate protein sequences for each chromosome in FASTA file in parallel
* Merge individual chromosome FASTA files

Notes:
This workflow expect a pygeno database in the current working directory with
a pre-loaded genome.

----------------------------------------------------------------------------------------

How to run:

Singlularity
* TAG=build ~/nextflow ~/code/pps/src/nf-workflows/personalized_proteome/main.nf -profile singularity -resume --genome_name="GRCh38.88 v2" --snp_name="1_0_SNPSet" --snp_quality_threshold=20 --protein_length_threshold=10000

Docker
WARNING: Docker profile is currently broken because of optimization
* sudo ~/nextflow ~/code/pps/src/nf-workflows/personalized_proteome/main.nf -profile docker -resume --genome_name="GRCh38.88 v2" --snp_name="1_0_SNPSet" --snp_quality_threshold=20 --protein_length_threshold=10000

----------------------------------------------------------------------------------------
*/


/*
 * Define the default parameters
 */

chromosomes_merge = Channel.of(1..30, 'X', 'Y', 'MT').map {it + '.fa'}
snp_packages = Channel.fromPath('*.tar.gz')
pygeno_data = Channel.fromPath('db/*.tar.gz')

/*
 * Check empty user provided parameters
 */

if(params.genome_name == null) {
    exit 1, "ERROR: genome_name is empty"
}

if(params.snp_name == null) {
    exit 1, "ERROR: snp_name is empty"
}

if(params.snp_quality_threshold == null) {
    exit 1, "ERROR: snp_quality_threshold is empty"
}

if(params.protein_length_threshold == null) {
    exit 1, "ERROR: protein_length_threshold is empty"
}

tpm_threshold = ""
if(params.tpm_threshold != null) {
    tpm_threshold = "--tpm-threshold=" + params.tpm_threshold
}

expression_channel = ""

expression_file = ""
if(params.expression_file != null) {
    expression_file = "--expression-file=\"" + params.expression_file + "\""
    expression_channel = Channel.fromPath(params.expression_file)
}


log.info """\
=====================================
 Personalized proteome Workflow
=====================================
 Genome name: ${params.genome_name}
 SNP name: ${params.snp_name}
 SNP quality threshold: ${params.snp_quality_threshold}
 Maximum protein length threshold: ${params.protein_length_threshold}
 TPM threshold: ${params.tpm_threshold}
 Expression file: ${params.expression_file}
 Fasta file name: ${params.fasta_file}
=====================================
"""

/**
 Import SNP set to pyGeno database
 Generate protein sequences for each chromosome in FASTA file in parallel
 */

process make_chromosome_fasta {

    container "$PPS_CONTAINER"
    memory = '500MB'

    scratch true

    errorStrategy 'retry'

    input:
    each file(snp_package) from snp_packages
    file db_file from pygeno_data
    each file(x) from expression_channel

    output:
    file '*.fa.zip' into fasta_files
    file 'import_snps_*.log'
    file 'error_*.txt'

    publishDir "${params.outdir}", pattern: 'import_snps_*.log'
    publishDir "${params.outdir}", pattern: 'error_*.txt'

    beforeScript 'chmod o+rw .'

    script:
    """

    chromosome=`echo $db_file | sed "s/db_//" | sed "s/.tar.gz//"`
    echo \$chromosome

    tar xvzf ${db_file}

    PYGENO_DATA_TMP=. python /usr/app/src/api/pygeno.py import_snps ${snp_package} \$chromosome 2>&1 | tee import_snps_\$chromosome.log

    PYGENO_DATA_TMP=. python /usr/app/src/api/pygeno.py \
    make_personalized_proteome \
    "${params.genome_name}" "${params.snp_name}" \$chromosome \
    ${params.snp_quality_threshold} \
    --protein-length-threshold ${params.protein_length_threshold} \
    ${tpm_threshold} \
    ${expression_file} \
    > \$chromosome.fa 2> error_\$chromosome.txt


    zip \$chromosome.fa.zip \$chromosome.fa

    """
}


/**
 Merge individual chromosome FASTA files to a single one
 */
process merge_fasta {

    container "$PPS_CONTAINER"
    memory = '250MB'

    scratch true

    errorStrategy 'retry'

    input:
    val chromosomes from chromosomes_merge.collect()
    file x from fasta_files.collect()

    output:
    file '*.fasta.zip'

    publishDir "${params.outdir}"

    script:
    """
    unzip '*.zip'
    cat `echo ${chromosomes} | sed "s/[][,]//g"` > ${params.fasta_file}.fasta 2>/dev/null | true

    reformat=${params.reformat}
    fasta_file=${params.fasta_file}

    cp \$fasta_file.fasta pp.fasta
    zip -9 pp.fasta.zip pp.fasta

    if \$reformat ; then
        python /usr/app/src/api/reformat.py -n ${params.sample_id} -pp ${params.fasta_file}.fasta  -o .
        fasta_file=${params.fasta_file}_reformat
    fi

    zip -9 \$fasta_file.fasta.zip \$fasta_file.fasta
    """
}


workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}